/******************************************************************************
 * Copyright 2016 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#ifndef HALMAC_INTF_PHY_CMD
#define HALMAC_INTF_PHY_CMD

/* Cut mask */
enum halmac_intf_phy_cut {
	HALMAC_INTF_PHY_CUT_TESTCHIP = BIT(0),
	HALMAC_INTF_PHY_CUT_A = BIT(1),
	HALMAC_INTF_PHY_CUT_B = BIT(2),
	HALMAC_INTF_PHY_CUT_C = BIT(3),
	HALMAC_INTF_PHY_CUT_D = BIT(4),
	HALMAC_INTF_PHY_CUT_E = BIT(5),
	HALMAC_INTF_PHY_CUT_F = BIT(6),
	HALMAC_INTF_PHY_CUT_G = BIT(7),
	HALMAC_INTF_PHY_CUT_ALL = 0x7FFF,
};

/* IP selection */
enum halmac_ip_sel {
	HALMAC_IP_INTF_PHY = 0,
	HALMAC_IP_SEL_MAC = 1,
	HALMAC_IP_PCIE_DBI = 2,
	HALMAC_IP_SEL_UNDEFINE = 0x7FFF,
};

/* Platform mask */
enum halmac_intf_phy_platform {
	HALMAC_INTF_PHY_PLATFORM_ALL = BIT(0),
	HALMAC_INTF_PHY_PLATFORM_ASUS = BIT(1),
	HALMAC_INTF_PHY_PLATFORM_FOR_ALL = 0x7FFF,
};

#endif
