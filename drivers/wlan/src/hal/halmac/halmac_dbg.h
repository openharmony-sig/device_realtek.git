/******************************************************************************
 * Copyright 2018 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#ifndef _HALMAC_DBG_H_
#define _HALMAC_DBG_H_

#include "halmac_api.h"

#if HALMAC_DBG_MONITOR_IO
enum halmac_ret_status
mount_api_dbg(struct halmac_adapter *adapter);
#endif

#endif
