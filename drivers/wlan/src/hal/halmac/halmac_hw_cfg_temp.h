/******************************************************************************
 * Copyright 2016 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#ifndef __HALMAC__HW_CFG_H__
#define __HALMAC__HW_CFG_H__

#define HALMAC_8723A_SUPPORT	0
#define HALMAC_8188E_SUPPORT	0
#define HALMAC_8821A_SUPPORT	0
#define HALMAC_8723B_SUPPORT	0
#define HALMAC_8812A_SUPPORT	0
#define HALMAC_8192E_SUPPORT	0
#define HALMAC_8881A_SUPPORT	0
#define HALMAC_8821B_SUPPORT	0
#define HALMAC_8814A_SUPPORT	0
#define HALMAC_8881A_SUPPORT	0
#define HALMAC_8703B_SUPPORT	0
#define HALMAC_8723D_SUPPORT	0
#define HALMAC_8188F_SUPPORT	0
#define HALMAC_8821BMP_SUPPORT	0
#define HALMAC_8814AMP_SUPPORT	0
#define HALMAC_8195A_SUPPORT	0
#define HALMAC_8821B_SUPPORT	0
#define HALMAC_8196F_SUPPORT	0
#define HALMAC_8197F_SUPPORT	0
#define HALMAC_8198F_SUPPORT	0
#define HALMAC_8192F_SUPPORT	0
#define HALMAC_8197G_SUPPORT	0

/* Halmac support IC version */
#define HALMAC_8814B_SUPPORT	1
#define HALMAC_8821C_SUPPORT	1
#define HALMAC_8822B_SUPPORT	1
#define HALMAC_8822C_SUPPORT	1
#define HALMAC_8812F_SUPPORT	1

/* Interface support */
#define HALMAC_SDIO_SUPPORT	1
#define HALMAC_USB_SUPPORT	1
#define HALMAC_PCIE_SUPPORT	1

#endif

