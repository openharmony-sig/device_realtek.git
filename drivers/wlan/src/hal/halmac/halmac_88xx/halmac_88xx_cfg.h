/******************************************************************************
 * Copyright 2016 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#ifndef _HALMAC_88XX_CFG_H_
#define _HALMAC_88XX_CFG_H_

#include "../halmac_api.h"

#if HALMAC_88XX_SUPPORT

#define TX_PAGE_SIZE_88XX		128
#define TX_PAGE_SIZE_SHIFT_88XX		7 /* 128 = 2^7 */
#define TX_ALIGN_SIZE_88XX		8
#define SDIO_TX_MAX_SIZE_88XX		31744
#define RX_BUF_FW_88XX			12288

#define TX_DESC_SIZE_88XX		48
#define RX_DESC_SIZE_88XX		24

#define H2C_PKT_SIZE_88XX		32 /* Only support 32 byte packet now */
#define H2C_PKT_HDR_SIZE_88XX		8
#define C2H_DATA_OFFSET_88XX		10
#define C2H_PKT_BUF_88XX		256

/* HW memory address */
#define OCPBASE_TXBUF_88XX		0x18780000
#define OCPBASE_DMEM_88XX		0x00200000
#define OCPBASE_EMEM_88XX		0x00100000

#endif /* HALMAC_88XX_SUPPORT */

#endif
