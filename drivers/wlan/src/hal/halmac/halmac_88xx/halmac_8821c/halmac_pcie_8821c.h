/******************************************************************************
 * Copyright 2016 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#ifndef _HALMAC_PCIE_8821C_H_
#define _HALMAC_PCIE_8821C_H_

#include "../../halmac_api.h"

#if (HALMAC_8821C_SUPPORT && HALMAC_PCIE_SUPPORT)

extern struct halmac_intf_phy_para pcie_gen1_phy_param_8821c[];
extern struct halmac_intf_phy_para pcie_gen2_phy_param_8821c[];

enum halmac_ret_status
mac_pwr_switch_pcie_8821c(struct halmac_adapter *adapter,
			  enum halmac_mac_power pwr);

enum halmac_ret_status
pcie_switch_8821c(struct halmac_adapter *adapter, enum halmac_pcie_cfg cfg);

enum halmac_ret_status
phy_cfg_pcie_8821c(struct halmac_adapter *adapter,
		   enum halmac_intf_phy_platform pltfm);

enum halmac_ret_status
intf_tun_pcie_8821c(struct halmac_adapter *adapter);

enum halmac_ret_status
auto_refclk_cal_8821c_pcie(struct halmac_adapter *adapter);

enum halmac_ret_status
cfgspc_set_pcie_8821c(struct halmac_adapter *adapter,
		      struct halmac_pcie_cfgspc_param *param);

#endif /* HALMAC_8821C_SUPPORT */

#endif/* _HALMAC_PCIE_8821C_H_ */
