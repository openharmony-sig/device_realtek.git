/******************************************************************************
 * Copyright 2016 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#ifndef _HALMAC_GPIO_8821C_H_
#define _HALMAC_GPIO_8821C_H_

#include "../../halmac_api.h"

#if HALMAC_8821C_SUPPORT

enum halmac_ret_status
pinmux_get_func_8821c(struct halmac_adapter *adapter,
		      enum halmac_gpio_func gpio_func, u8 *enable);

enum halmac_ret_status
pinmux_set_func_8821c(struct halmac_adapter *adapter,
		      enum halmac_gpio_func gpio_func);

enum halmac_ret_status
pinmux_free_func_8821c(struct halmac_adapter *adapter,
		       enum halmac_gpio_func gpio_func);

#endif /* HALMAC_8821C_SUPPORT */

#endif/* _HALMAC_GPIO_8821C_H_ */
