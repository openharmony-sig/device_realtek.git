/******************************************************************************
 * Copyright 2017 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#ifndef _HALMAC_CFG_WMAC_8821C_H_
#define _HALMAC_CFG_WMAC_8821C_H_

#include "../../halmac_api.h"

#if HALMAC_8821C_SUPPORT

enum halmac_ret_status
cfg_drv_info_8821c(struct halmac_adapter *adapter,
		   enum halmac_drv_info drv_info);

enum halmac_ret_status
init_low_pwr_8821c(struct halmac_adapter *adapter);

void
cfg_rx_ignore_8821c(struct halmac_adapter *adapter,
		    struct halmac_mac_rx_ignore_cfg *cfg);

enum halmac_ret_status
cfg_ampdu_8821c(struct halmac_adapter *adapter,
		struct halmac_ampdu_config *cfg);

#endif/* HALMAC_8821C_SUPPORT */

#endif/* _HALMAC_CFG_WMAC_8821C_H_ */
