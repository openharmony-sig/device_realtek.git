/******************************************************************************
 * Copyright 2016 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#ifndef HALMAC_POWER_SEQUENCE_8821C
#define HALMAC_POWER_SEQUENCE_8821C

#include "../../halmac_pwr_seq_cmd.h"
#include "../../halmac_hw_cfg.h"

#if HALMAC_8821C_SUPPORT

#define HALMAC_8821C_PWR_SEQ_VER  "V20"

extern struct halmac_wlan_pwr_cfg *card_dis_flow_8821c[];
extern struct halmac_wlan_pwr_cfg *card_en_flow_8821c[];
extern struct halmac_wlan_pwr_cfg *suspend_flow_8821c[];
extern struct halmac_wlan_pwr_cfg *resume_flow_8821c[];
extern struct halmac_wlan_pwr_cfg *hwpdn_flow_8821c[];
extern struct halmac_wlan_pwr_cfg *enter_lps_flow_8821c[];
extern struct halmac_wlan_pwr_cfg *enter_dlps_flow_8821c[];
extern struct halmac_wlan_pwr_cfg *leave_lps_flow_8821c[];

#endif /* HALMAC_8821C_SUPPORT */

#endif
