/******************************************************************************
 * Copyright 2016 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#ifndef _HALMAC_FW_88XX_H_
#define _HALMAC_FW_88XX_H_

#include "../halmac_api.h"

#if HALMAC_88XX_SUPPORT

#define HALMC_DDMA_POLLING_COUNT		1000

enum halmac_ret_status
download_firmware_88xx(struct halmac_adapter *adapter, u8 *fw_bin, u32 size);

enum halmac_ret_status
free_download_firmware_88xx(struct halmac_adapter *adapter,
			    enum halmac_dlfw_mem mem_sel, u8 *fw_bin, u32 size);

enum halmac_ret_status
reset_wifi_fw_88xx(struct halmac_adapter *adapter);

enum halmac_ret_status
get_fw_version_88xx(struct halmac_adapter *adapter,
		    struct halmac_fw_version *ver);

enum halmac_ret_status
check_fw_status_88xx(struct halmac_adapter *adapter, u8 *fw_status);

enum halmac_ret_status
dump_fw_dmem_88xx(struct halmac_adapter *adapter, u8 *dmem, u32 *size);

enum halmac_ret_status
cfg_max_dl_size_88xx(struct halmac_adapter *adapter, u32 size);

enum halmac_ret_status
enter_cpu_sleep_mode_88xx(struct halmac_adapter *adapter);

enum halmac_ret_status
get_cpu_mode_88xx(struct halmac_adapter *adapter,
		  enum halmac_wlcpu_mode *mode);

enum halmac_ret_status
send_general_info_88xx(struct halmac_adapter *adapter,
		       struct halmac_general_info *info);

enum halmac_ret_status
drv_fwctrl_88xx(struct halmac_adapter *adapter, u8 *payload, u32 size, u8 ack);

#endif /* HALMAC_88XX_SUPPORT */

#endif/* _HALMAC_FW_88XX_H_ */
