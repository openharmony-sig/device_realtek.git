/******************************************************************************
 * Copyright 2016 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#ifndef _HALMAC_GPIO_88XX_H_
#define _HALMAC_GPIO_88XX_H_

#include "../halmac_api.h"
#include "../halmac_gpio_cmd.h"

#if HALMAC_88XX_SUPPORT

enum halmac_ret_status
pinmux_wl_led_mode_88xx(struct halmac_adapter *adapter,
			enum halmac_wlled_mode mode);

void
pinmux_wl_led_sw_ctrl_88xx(struct halmac_adapter *adapter, u8 on);

void
pinmux_sdio_int_polarity_88xx(struct halmac_adapter *adapter, u8 low_active);

enum halmac_ret_status
pinmux_gpio_mode_88xx(struct halmac_adapter *adapter, u8 gpio_id, u8 output);

enum halmac_ret_status
pinmux_gpio_output_88xx(struct halmac_adapter *adapter, u8 gpio_id, u8 high);

enum halmac_ret_status
pinmux_pin_status_88xx(struct halmac_adapter *adapter, u8 pin_id, u8 *high);

enum halmac_ret_status
pinmux_parser_88xx(struct halmac_adapter *adapter,
		   const struct halmac_gpio_pimux_list *list, u32 size,
		   u32 gpio_id, u32 *cur_func);

#endif /* HALMAC_88XX_SUPPORT */

#endif/* _HALMAC_GPIO_88XX_H_ */
