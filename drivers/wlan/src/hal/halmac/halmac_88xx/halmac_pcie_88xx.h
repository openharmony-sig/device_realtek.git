/******************************************************************************
 * Copyright 2016 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#ifndef _HALMAC_PCIE_88XX_H_
#define _HALMAC_PCIE_88XX_H_

#include "../halmac_api.h"

#if (HALMAC_88XX_SUPPORT && HALMAC_PCIE_SUPPORT)

enum halmac_ret_status
init_pcie_cfg_88xx(struct halmac_adapter *adapter);

enum halmac_ret_status
deinit_pcie_cfg_88xx(struct halmac_adapter *adapter);

enum halmac_ret_status
cfg_pcie_rx_agg_88xx(struct halmac_adapter *adapter,
		     struct halmac_rxagg_cfg *cfg);

u8
reg_r8_pcie_88xx(struct halmac_adapter *adapter, u32 offset);

enum halmac_ret_status
reg_w8_pcie_88xx(struct halmac_adapter *adapter, u32 offset, u8 value);

u16
reg_r16_pcie_88xx(struct halmac_adapter *adapter, u32 offset);

enum halmac_ret_status
reg_w16_pcie_88xx(struct halmac_adapter *adapter, u32 offset, u16 value);

u32
reg_r32_pcie_88xx(struct halmac_adapter *adapter, u32 offset);

enum halmac_ret_status
reg_w32_pcie_88xx(struct halmac_adapter *adapter, u32 offset, u32 value);

enum halmac_ret_status
cfg_txagg_pcie_align_88xx(struct halmac_adapter *adapter, u8 enable,
			  u16 align_size);

enum halmac_ret_status
tx_allowed_pcie_88xx(struct halmac_adapter *adapter, u8 *buf, u32 size);

u32
pcie_indirect_reg_r32_88xx(struct halmac_adapter *adapter, u32 offset);

enum halmac_ret_status
pcie_reg_rn_88xx(struct halmac_adapter *adapter, u32 offset, u32 size,
		 u8 *value);

enum halmac_ret_status
set_pcie_bulkout_num_88xx(struct halmac_adapter *adapter, u8 num);

enum halmac_ret_status
get_pcie_tx_addr_88xx(struct halmac_adapter *adapter, u8 *buf, u32 size,
		      u32 *cmd53_addr);

enum halmac_ret_status
get_pcie_bulkout_id_88xx(struct halmac_adapter *adapter, u8 *buf, u32 size,
			 u8 *id);

enum halmac_ret_status
mdio_write_88xx(struct halmac_adapter *adapter, u8 addr, u16 data, u8 speed);

u16
mdio_read_88xx(struct halmac_adapter *adapter, u8 addr, u8 speed);

enum halmac_ret_status
dbi_w32_88xx(struct halmac_adapter *adapter, u16 addr, u32 data);

u32
dbi_r32_88xx(struct halmac_adapter *adapter, u16 addr);

enum halmac_ret_status
dbi_w8_88xx(struct halmac_adapter *adapter, u16 addr, u8 data);

u8
dbi_r8_88xx(struct halmac_adapter *adapter, u16 addr);

enum halmac_ret_status
trxdma_check_idle_88xx(struct halmac_adapter *adapter);

enum halmac_ret_status
en_ref_autok_pcie_88xx(struct halmac_adapter *dapter, u8 en);

#endif /* HALMAC_88XX_SUPPORT */

#endif/* _HALMAC_PCIE_88XX_H_ */
