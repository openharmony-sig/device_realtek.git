/******************************************************************************
 * Copyright 2016 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#ifndef _HALMAC_SDIO_88XX_H_
#define _HALMAC_SDIO_88XX_H_

#include "../halmac_api.h"

#if (HALMAC_88XX_SUPPORT && HALMAC_SDIO_SUPPORT)

enum halmac_ret_status
init_sdio_cfg_88xx(struct halmac_adapter *adapter);

enum halmac_ret_status
deinit_sdio_cfg_88xx(struct halmac_adapter *adapter);

enum halmac_ret_status
cfg_sdio_rx_agg_88xx(struct halmac_adapter *adapter,
		     struct halmac_rxagg_cfg *cfg);

enum halmac_ret_status
cfg_txagg_sdio_align_88xx(struct halmac_adapter *adapter, u8 enable,
			  u16 align_size);

u32
sdio_indirect_reg_r32_88xx(struct halmac_adapter *adapter, u32 offset);

enum halmac_ret_status
sdio_reg_rn_88xx(struct halmac_adapter *adapter, u32 offset, u32 size,
		 u8 *value);

enum halmac_ret_status
set_sdio_bulkout_num_88xx(struct halmac_adapter *adapter, u8 num);

enum halmac_ret_status
get_sdio_bulkout_id_88xx(struct halmac_adapter *adapter, u8 *buf, u32 size,
			 u8 *id);

enum halmac_ret_status
sdio_cmd53_4byte_88xx(struct halmac_adapter *adapter,
		      enum halmac_sdio_cmd53_4byte_mode mode);

enum halmac_ret_status
sdio_hw_info_88xx(struct halmac_adapter *adapter,
		  struct halmac_sdio_hw_info *info);

void
cfg_sdio_tx_page_threshold_88xx(struct halmac_adapter *adapter,
				struct halmac_tx_page_threshold_info *info);

enum halmac_ret_status
cnv_to_sdio_bus_offset_88xx(struct halmac_adapter *adapter, u32 *offset);

enum halmac_ret_status
leave_sdio_suspend_88xx(struct halmac_adapter *adapter);

u32
r_indir_sdio_88xx(struct halmac_adapter *adapter, u32 adr,
		  enum halmac_io_size size);

enum halmac_ret_status
w_indir_sdio_88xx(struct halmac_adapter *adapter, u32 adr, u32 val,
		  enum halmac_io_size size);

enum halmac_ret_status
en_ref_autok_sdio_88xx(struct halmac_adapter *adapter, u8 en);

#endif /* HALMAC_88XX_SUPPORT */

#endif/* _HALMAC_SDIO_88XX_H_ */
