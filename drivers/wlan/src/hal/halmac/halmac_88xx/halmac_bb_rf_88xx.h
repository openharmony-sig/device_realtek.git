/******************************************************************************
 * Copyright 2016 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#ifndef _HALMAC_BB_RF_88XX_H_
#define _HALMAC_BB_RF_88XX_H_

#include "../halmac_api.h"

#if HALMAC_88XX_SUPPORT

enum halmac_ret_status
start_iqk_88xx(struct halmac_adapter *adapter, struct halmac_iqk_para *param);

enum halmac_ret_status
ctrl_pwr_tracking_88xx(struct halmac_adapter *adapter,
		       struct halmac_pwr_tracking_option *opt);

enum halmac_ret_status
get_iqk_status_88xx(struct halmac_adapter *adapter,
		    enum halmac_cmd_process_status *proc_status);

enum halmac_ret_status
get_pwr_trk_status_88xx(struct halmac_adapter *adapter,
			enum halmac_cmd_process_status *proc_status);

enum halmac_ret_status
get_psd_status_88xx(struct halmac_adapter *adapter,
		    enum halmac_cmd_process_status *proc_status, u8 *data,
		    u32 *size);

enum halmac_ret_status
psd_88xx(struct halmac_adapter *adapter, u16 start_psd, u16 end_psd);

enum halmac_ret_status
get_h2c_ack_iqk_88xx(struct halmac_adapter *adapter, u8 *buf, u32 size);

enum halmac_ret_status
get_h2c_ack_pwr_trk_88xx(struct halmac_adapter *adapter, u8 *buf, u32 size);

enum halmac_ret_status
get_psd_data_88xx(struct halmac_adapter *adapter, u8 *buf, u32 size);

#endif /* HALMAC_88XX_SUPPORT */

#endif/* _HALMAC_BB_RF_88XX_H_ */
