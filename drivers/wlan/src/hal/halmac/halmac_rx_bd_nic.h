/******************************************************************************
 * Copyright 2016 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#ifndef _HALMAC_RX_BD_NIC_H_
#define _HALMAC_RX_BD_NIC_H_
#if (HALMAC_8814A_SUPPORT || HALMAC_8822B_SUPPORT || HALMAC_8197F_SUPPORT ||   \
	HALMAC_8821C_SUPPORT || HALMAC_8198F_SUPPORT || HALMAC_8822C_SUPPORT ||\
	HALMAC_8192F_SUPPORT || HALMAC_8197G_SUPPORT || HALMAC_8814B_SUPPORT)

/*TXBD_DW0*/

#define GET_RX_BD_RXFAIL(rxbd) LE_BITS_TO_4BYTE(rxbd + 0x00, 31, 1)
#define GET_RX_BD_TOTALRXPKTSIZE(rxbd) LE_BITS_TO_4BYTE(rxbd + 0x00, 16, 13)
#define GET_RX_BD_RXTAG(rxbd) LE_BITS_TO_4BYTE(rxbd + 0x00, 16, 13)
#define GET_RX_BD_FS(rxbd) LE_BITS_TO_4BYTE(rxbd + 0x00, 15, 1)
#define GET_RX_BD_LS(rxbd) LE_BITS_TO_4BYTE(rxbd + 0x00, 14, 1)
#define GET_RX_BD_RXBUFFSIZE(rxbd) LE_BITS_TO_4BYTE(rxbd + 0x00, 0, 14)

/*TXBD_DW1*/

#define GET_RX_BD_PHYSICAL_ADDR_LOW(rxbd) LE_BITS_TO_4BYTE(rxbd + 0x04, 0, 32)

/*TXBD_DW2*/

#define GET_RX_BD_PHYSICAL_ADDR_HIGH(rxbd) LE_BITS_TO_4BYTE(rxbd + 0x08, 0, 32)

#endif

#endif
