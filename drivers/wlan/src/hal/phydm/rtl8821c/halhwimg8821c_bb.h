/******************************************************************************
 * Copyright 2007 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

/*Image2HeaderVersion: R3 1.5.5*/
#if (RTL8821C_SUPPORT == 1)
#ifndef __INC_MP_BB_HW_IMG_8821C_H
#define __INC_MP_BB_HW_IMG_8821C_H

/******************************************************************************
 *                           agc_tab.TXT
 ******************************************************************************/

/* tc: Test Chip, mp: mp Chip*/
void
odm_read_and_config_mp_8821c_agc_tab(struct dm_struct *dm);
u32 odm_get_version_mp_8821c_agc_tab(void);

/******************************************************************************
 *                           agc_tab_diff.TXT
 ******************************************************************************/

extern const u32	array_mp_8821c_agc_tab_diff_wlg[780];
extern const u32	array_mp_8821c_agc_tab_diff_btg[780];
void
odm_read_and_config_mp_8821c_agc_tab_diff(struct dm_struct *dm,
					  const u32 array[],
					  u32 array_len);
u32 odm_get_version_mp_8821c_agc_tab_diff(void);

/******************************************************************************
 *                           phy_reg.TXT
 ******************************************************************************/

/* tc: Test Chip, mp: mp Chip*/
void
odm_read_and_config_mp_8821c_phy_reg(struct dm_struct *dm);
u32 odm_get_version_mp_8821c_phy_reg(void);

/******************************************************************************
 *                           phy_reg_mp.TXT
 ******************************************************************************/

/* tc: Test Chip, mp: mp Chip*/
void
odm_read_and_config_mp_8821c_phy_reg_mp(struct dm_struct *dm);
u32 odm_get_version_mp_8821c_phy_reg_mp(void);

/******************************************************************************
 *                           phy_reg_pg.TXT
 ******************************************************************************/

/* tc: Test Chip, mp: mp Chip*/
void
odm_read_and_config_mp_8821c_phy_reg_pg(struct dm_struct *dm);
u32 odm_get_version_mp_8821c_phy_reg_pg(void);

/******************************************************************************
 *                           phy_reg_pg_type0x28.TXT
 ******************************************************************************/

/* tc: Test Chip, mp: mp Chip*/
void
odm_read_and_config_mp_8821c_phy_reg_pg_type0x28(struct dm_struct *dm);
u32 odm_get_version_mp_8821c_phy_reg_pg_type0x28(void);

#endif
#endif /* end of HWIMG_SUPPORT*/

