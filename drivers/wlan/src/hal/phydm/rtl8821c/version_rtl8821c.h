/******************************************************************************
 * Copyright 2007 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/
/*RTL8821C PHY Parameters*/
/*
[Caution]
  Since 01/Aug/2015, the commit rules will be simplified.
  You do not need to fill up the version.h anymore,
  only the maintenance supervisor fills it before formal release.
*/
#define	RELEASE_DATE_8821C		20190305
#define	COMMIT_BY_8821C			"Coiln"
#define	RELEASE_VERSION_8821C	54
