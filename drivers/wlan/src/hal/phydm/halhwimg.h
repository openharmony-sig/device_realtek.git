/******************************************************************************
 * Copyright 2016 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/
#pragma once
#ifndef __INC_HW_IMG_H
#define __INC_HW_IMG_H

/*@
 * 2011/03/15 MH Add for different IC HW image file selection. code size consideration.
 *   */
#if RT_PLATFORM == PLATFORM_LINUX

	#if (DEV_BUS_TYPE == RT_PCI_INTERFACE)
		/* @For 92C */
		#define		RTL8192CE_HWIMG_SUPPORT					1
		#define		RTL8192CE_TEST_HWIMG_SUPPORT			0
		#define		RTL8192CU_HWIMG_SUPPORT					0
		#define		RTL8192CU_TEST_HWIMG_SUPPORT			0

		/* @For 92D */
		#define		RTL8192DE_HWIMG_SUPPORT					1
		#define		RTL8192DE_TEST_HWIMG_SUPPORT			0
		#define		RTL8192DU_HWIMG_SUPPORT					0
		#define		RTL8192DU_TEST_HWIMG_SUPPORT			0

		/* @For 8723 */
		#define		RTL8723E_HWIMG_SUPPORT					1
		#define		RTL8723U_HWIMG_SUPPORT					0
		#define		RTL8723S_HWIMG_SUPPORT					0

		/* @For 88E */
		#define		RTL8188EE_HWIMG_SUPPORT					0
		#define		RTL8188EU_HWIMG_SUPPORT					0
		#define		RTL8188ES_HWIMG_SUPPORT					0

	#elif (DEV_BUS_TYPE == RT_USB_INTERFACE)
		/* @For 92C */
		#define	RTL8192CE_HWIMG_SUPPORT				0
		#define	RTL8192CE_TEST_HWIMG_SUPPORT			0
		#define	RTL8192CU_HWIMG_SUPPORT				1
		#define	RTL8192CU_TEST_HWIMG_SUPPORT			0

		/* @For 92D */
		#define	RTL8192DE_HWIMG_SUPPORT				0
		#define	RTL8192DE_TEST_HWIMG_SUPPORT			0
		#define	RTL8192DU_HWIMG_SUPPORT				1
		#define	RTL8192DU_TEST_HWIMG_SUPPORT			0

		/* @For 8723 */
		#define	RTL8723E_HWIMG_SUPPORT					0
		#define	RTL8723U_HWIMG_SUPPORT					1
		#define	RTL8723S_HWIMG_SUPPORT					0

		/* @For 88E */
		#define		RTL8188EE_HWIMG_SUPPORT					0
		#define		RTL8188EU_HWIMG_SUPPORT					0
		#define		RTL8188ES_HWIMG_SUPPORT					0

	#elif (DEV_BUS_TYPE == RT_SDIO_INTERFACE)
		/* @For 92C */
		#define	RTL8192CE_HWIMG_SUPPORT				0
		#define	RTL8192CE_TEST_HWIMG_SUPPORT			0
		#define	RTL8192CU_HWIMG_SUPPORT				1
		#define	RTL8192CU_TEST_HWIMG_SUPPORT			0

		/* @For 92D */
		#define	RTL8192DE_HWIMG_SUPPORT				0
		#define	RTL8192DE_TEST_HWIMG_SUPPORT			0
		#define	RTL8192DU_HWIMG_SUPPORT				1
		#define	RTL8192DU_TEST_HWIMG_SUPPORT			0

		/* @For 8723 */
		#define	RTL8723E_HWIMG_SUPPORT					0
		#define	RTL8723U_HWIMG_SUPPORT					0
		#define	RTL8723S_HWIMG_SUPPORT					1

		/* @For 88E */
		#define		RTL8188EE_HWIMG_SUPPORT					0
		#define		RTL8188EU_HWIMG_SUPPORT					0
		#define		RTL8188ES_HWIMG_SUPPORT					0
	#endif

#else	/* PLATFORM_WINDOWS & MacOSX */

	/* @For 92C */
	#define		RTL8192CE_HWIMG_SUPPORT						1
	#define		RTL8192CE_TEST_HWIMG_SUPPORT				1
	#define		RTL8192CU_HWIMG_SUPPORT						1
	#define		RTL8192CU_TEST_HWIMG_SUPPORT				1

	/* @For 92D */
	#define		RTL8192DE_HWIMG_SUPPORT					1
	#define		RTL8192DE_TEST_HWIMG_SUPPORT				1
	#define		RTL8192DU_HWIMG_SUPPORT					1
	#define		RTL8192DU_TEST_HWIMG_SUPPORT				1

	#if defined(UNDER_CE)
		/* @For 8723 */
		#define		RTL8723E_HWIMG_SUPPORT					0
		#define		RTL8723U_HWIMG_SUPPORT					0
		#define		RTL8723S_HWIMG_SUPPORT					1

		/* @For 88E */
		#define		RTL8188EE_HWIMG_SUPPORT					0
		#define		RTL8188EU_HWIMG_SUPPORT					0
		#define		RTL8188ES_HWIMG_SUPPORT					0

	#else

		/* @For 8723 */
		#define		RTL8723E_HWIMG_SUPPORT					1
		/* @#define		RTL_8723E_TEST_HWIMG_SUPPORT			1 */
		#define		RTL8723U_HWIMG_SUPPORT					1
		/* @#define		RTL_8723U_TEST_HWIMG_SUPPORT			1 */
		#define		RTL8723S_HWIMG_SUPPORT					1
		/* @#define		RTL_8723S_TEST_HWIMG_SUPPORT			1 */

		/* @For 88E */
		#define		RTL8188EE_HWIMG_SUPPORT					1
		#define		RTL8188EU_HWIMG_SUPPORT					1
		#define		RTL8188ES_HWIMG_SUPPORT					1
	#endif

#endif

#endif /* @__INC_HW_IMG_H */
