/******************************************************************************
 * Copyright 2007 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/
#ifndef __ODMNOISEMONITOR_H__
#define __ODMNOISEMONITOR_H__

#define VALID_CNT 5

struct noise_level {
	u8 value[PHYDM_MAX_RF_PATH];
	s8 sval[PHYDM_MAX_RF_PATH];
	s32 sum[PHYDM_MAX_RF_PATH];
	u8 valid[PHYDM_MAX_RF_PATH];
	u8 valid_cnt[PHYDM_MAX_RF_PATH];
};

struct odm_noise_monitor {
	s8 noise[PHYDM_MAX_RF_PATH];
	s16 noise_all;
};

s16 odm_inband_noise_monitor(void *dm_void, u8 is_pause_dig, u8 igi_value,
			     u32 max_time);

void phydm_noisy_detection(void *dm_void);

#endif
