/******************************************************************************
 * Copyright 2007 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#ifndef __PHYDM_POW_TRAIN_H__
#define __PHYDM_POW_TRAIN_H__

#define POW_TRAIN_VERSION "1.0" /* @2017.07.0141  Dino, Add phydm_pow_train.h*/

/****************************************************************
 * 1 ============================================================
 * 1  Definition
 * 1 ============================================================
 ***************************************************************/

#ifdef PHYDM_POWER_TRAINING_SUPPORT
/****************************************************************
 * 1 ============================================================
 * 1  structure
 * 1 ============================================================
 ***************************************************************/

struct phydm_pow_train_stuc {
	u8 pt_state;
	u32 pow_train_score;
};

/****************************************************************
 * 1 ============================================================
 * 1  enumeration
 * 1 ============================================================
 ***************************************************************/

enum pow_train_state {
	DYNAMIC_POW_TRAIN = 0,
	ENABLE_POW_TRAIN = 1,
	DISABLE_POW_TRAIN = 2
};

enum power_training_score {
	DISABLE_PT_SCORE = 0,
	KEEP_PRE_PT_SCORE = 1,
	ENABLE_PT_SCORE = 2
};

/****************************************************************
 * 1 ============================================================
 * 1  function prototype
 * 1 ============================================================
 ***************************************************************/

void phydm_update_power_training_state(
	void *dm_void);

void phydm_pow_train_debug(
	void *dm_void,
	char input[][16],
	u32 *_used,
	char *output,
	u32 *_out_len);

#endif
#endif
