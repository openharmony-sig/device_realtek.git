/******************************************************************************
 * Copyright 2007 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#ifndef __HALRF_POWER_TRACKING_H__
#define __HALRF_POWER_TRACKING_H__

boolean
odm_check_power_status(void *dm_void);

#if (DM_ODM_SUPPORT_TYPE & (ODM_WIN | ODM_CE))
void halrf_update_pwr_track(void *dm_void, u8 rate);
#endif

#if (DM_ODM_SUPPORT_TYPE == ODM_WIN)
void halrf_update_init_rate_work_item_callback(
	void *context);
#endif

void halrf_set_pwr_track(void *dm_void, u8 enable);

#endif /*#ifndef __HALRF_POWERTRACKING_H__*/
