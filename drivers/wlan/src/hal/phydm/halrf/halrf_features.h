/******************************************************************************
 * Copyright 2007 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#ifndef __HALRF_FEATURES_H__
#define __HALRF_FEATURES_H__

#if (DM_ODM_SUPPORT_TYPE == ODM_WIN)

#define CONFIG_HALRF_POWERTRACKING 1

#elif (DM_ODM_SUPPORT_TYPE == ODM_AP)

#define CONFIG_HALRF_POWERTRACKING 1

#elif (DM_ODM_SUPPORT_TYPE == ODM_CE)

#define CONFIG_HALRF_POWERTRACKING 1

#endif

#endif /*#ifndef __HALRF_FEATURES_H__*/
