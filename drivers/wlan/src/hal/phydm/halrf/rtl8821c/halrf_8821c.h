/******************************************************************************
 * Copyright 2007 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#ifndef __HALRF_8821C_H__
#define __HALRF_8821C_H__

#define AVG_THERMAL_NUM_8821C 4
#define RF_T_METER_8821C 0x42

void configure_txpower_track_8821c(struct txpwrtrack_cfg *config);

void odm_tx_pwr_track_set_pwr8821c(void *dm_void, enum pwrtrack_method method,
				   u8 rf_path, u8 channel_mapped_index);

void get_delta_swing_table_8821c(void *dm_void,
#if (DM_ODM_SUPPORT_TYPE & ODM_AP)
				 u8 **temperature_up_a, u8 **temperature_down_a,
				 u8 **temperature_up_b, u8 **temperature_down_b,
				 u8 **temperature_up_cck_a,
				 u8 **temperature_down_cck_a,
				 u8 **temperature_up_cck_b,
				 u8 **temperature_down_cck_b
#else
				 u8 **temperature_up_a, u8 **temperature_down_a,
				 u8 **temperature_up_b,
				 u8 **temperature_down_b
#endif
				 );

void phy_lc_calibrate_8821c(void *dm_void);

void halrf_rf_lna_setting_8821c(struct dm_struct *dm, enum halrf_lna_set type);

#if ((DM_ODM_SUPPORT_TYPE & ODM_AP) || (DM_ODM_SUPPORT_TYPE == ODM_CE))
void phy_set_rf_path_switch_8821c(struct dm_struct *dm,
#else
void phy_set_rf_path_switch_8821c(void *adapter,
#endif
				  boolean is_main);

#if (DM_ODM_SUPPORT_TYPE & ODM_AP)
boolean phy_query_rf_path_switch_8821c(struct dm_struct *dm
#else
boolean phy_query_rf_path_switch_8821c(void *adapter
#endif
				       );

#endif /*#ifndef __HALRF_8821C_H__*/
