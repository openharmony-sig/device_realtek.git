/******************************************************************************
 * Copyright 2007 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

/*Image2HeaderVersion: R3 1.5.5*/
#if (RTL8821C_SUPPORT == 1)
#ifndef __INC_MP_RF_HW_IMG_8821C_H
#define __INC_MP_RF_HW_IMG_8821C_H

/* Please add following compiler flags definition (#define CONFIG_XXX_DRV_DIS)
 * into driver source code to reduce code size if necessary.
 * #define CONFIG_8821C_DRV_DIS
 * #define CONFIG_8821C_TYPE0X20_DRV_DIS
 * #define CONFIG_8821C_TYPE0X28_DRV_DIS
 * #define CONFIG_8821C_FCCSAR_DRV_DIS
 */

#define CONFIG_8821C
#ifdef CONFIG_8821C_DRV_DIS
    #undef CONFIG_8821C
#endif

#define CONFIG_8821C_TYPE0X20
#ifdef CONFIG_8821C_TYPE0X20_DRV_DIS
    #undef CONFIG_8821C_TYPE0X20
#endif

#define CONFIG_8821C_TYPE0X28
#ifdef CONFIG_8821C_TYPE0X28_DRV_DIS
    #undef CONFIG_8821C_TYPE0X28
#endif

#define CONFIG_8821C_FCCSAR
#ifdef CONFIG_8821C_FCCSAR_DRV_DIS
    #undef CONFIG_8821C_FCCSAR
#endif

/******************************************************************************
 *                           radioa.TXT
 ******************************************************************************/

/* tc: Test Chip, mp: mp Chip*/
void
odm_read_and_config_mp_8821c_radioa(struct dm_struct *dm);
u32 odm_get_version_mp_8821c_radioa(void);

/******************************************************************************
 *                           txpowertrack.TXT
 ******************************************************************************/

/* tc: Test Chip, mp: mp Chip*/
void
odm_read_and_config_mp_8821c_txpowertrack(struct dm_struct *dm);
u32 odm_get_version_mp_8821c_txpowertrack(void);

/******************************************************************************
 *                           txpowertrack_type0x20.TXT
 ******************************************************************************/

/* tc: Test Chip, mp: mp Chip*/
void
odm_read_and_config_mp_8821c_txpowertrack_type0x20(struct dm_struct *dm);
u32 odm_get_version_mp_8821c_txpowertrack_type0x20(void);

/******************************************************************************
 *                           txpowertrack_type0x28.TXT
 ******************************************************************************/

/* tc: Test Chip, mp: mp Chip*/
void
odm_read_and_config_mp_8821c_txpowertrack_type0x28(struct dm_struct *dm);
u32 odm_get_version_mp_8821c_txpowertrack_type0x28(void);

/******************************************************************************
 *                           txpwr_lmt.TXT
 ******************************************************************************/

/* tc: Test Chip, mp: mp Chip*/
void
odm_read_and_config_mp_8821c_txpwr_lmt(struct dm_struct *dm);
u32 odm_get_version_mp_8821c_txpwr_lmt(void);

/******************************************************************************
 *                           txpwr_lmt_fccsar.TXT
 ******************************************************************************/

/* tc: Test Chip, mp: mp Chip*/
void
odm_read_and_config_mp_8821c_txpwr_lmt_fccsar(struct dm_struct *dm);
u32 odm_get_version_mp_8821c_txpwr_lmt_fccsar(void);

#endif
#endif /* end of HWIMG_SUPPORT*/

