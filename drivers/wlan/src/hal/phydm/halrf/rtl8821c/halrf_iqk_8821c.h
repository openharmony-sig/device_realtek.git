/******************************************************************************
 * Copyright 2007 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#ifndef __HALRF_IQK_8821C_H__
#define __HALRF_IQK_8821C_H__

#if (RTL8821C_SUPPORT == 1)
/*============================================================*/
/*Definition */
/*============================================================*/

/*--------------------------Define Parameters-------------------------------*/
#define MAC_REG_NUM_8821C 3
#define BB_REG_NUM_8821C 10
#define RF_REG_NUM_8821C 5
#define DPK_BB_REG_NUM_8821C 23
#define DPK_BACKUP_REG_NUM_8821C 3

#define LOK_delay_8821C 2
#define GS_delay_8821C 2
#define WBIQK_delay_8821C 2

#define TXIQK 0
#define RXIQK 1
#define SS_8821C 1

/*---------------------------End Define Parameters-------------------------------*/

#if !(DM_ODM_SUPPORT_TYPE & ODM_AP)
void do_iqk_8821c(void *dm_void, u8 delta_thermal_index, u8 thermal_value,
		  u8 threshold);
#else
void do_iqk_8821c(void *dm_void, u8 delta_thermal_index, u8 thermal_value,
		  u8 threshold);
#endif

void phy_iq_calibrate_8821c(void *dm_void, boolean clear, boolean segment_iqk);

void phy_dp_calibrate_8821c(void *dm_void, boolean clear);

void do_imr_test_8821c(void *dm_void);

#else /* (RTL8821C_SUPPORT == 0)*/

#define phy_iq_calibrate_8821c(_pdm_void, clear, segment_iqk)
#define phy_dp_calibrate_8821c(_pDM_VOID, clear)

#endif /* RTL8821C_SUPPORT */

#endif /*#ifndef __HALRF_IQK_8821C_H__*/
