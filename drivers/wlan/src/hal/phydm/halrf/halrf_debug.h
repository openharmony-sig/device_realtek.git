/******************************************************************************
 * Copyright 2007 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#ifndef __HALRF_DEBUG_H__
#define __HALRF_DEBUG_H__

/*@============================================================*/
/*@include files*/
/*@============================================================*/

/*@============================================================*/
/*@Definition */
/*@============================================================*/

#if DBG

#if (DM_ODM_SUPPORT_TYPE == ODM_AP)
#define RF_DBG(dm, comp, fmt, args...)                     \
	do {                                               \
		if ((comp) & dm->rf_table.rf_dbg_comp) { \
			pr_debug("[RF] ");                 \
			RT_PRINTK(fmt, ##args);            \
		}                                          \
	} while (0)

#elif (DM_ODM_SUPPORT_TYPE == ODM_WIN)

static __inline void RF_DBG(PDM_ODM_T dm, int comp, char *fmt, ...)
{
	RT_STATUS rt_status;
	va_list args;
	char buf[PRINT_MAX_SIZE] = {0};

	if ((comp & dm->rf_table.rf_dbg_comp) == 0)
		return;

	if (fmt == NULL)
		return;

	va_start(args, fmt);
	rt_status = (RT_STATUS)RtlStringCbVPrintfA(buf, PRINT_MAX_SIZE, fmt, args);
	va_end(args);

	if (rt_status != RT_STATUS_SUCCESS) {
		DbgPrint("Failed (%d) to print message to buffer\n", rt_status);
		return;
	}

	DbgPrint("[RF] %s", buf);
}

#elif (DM_ODM_SUPPORT_TYPE == ODM_IOT)

#define RF_DBG(dm, comp, fmt, args...)                     \
	do {                                               \
		if ((comp) & dm->rf_table.rf_dbg_comp) { \
			RT_DEBUG(COMP_PHYDM, DBG_DMESG, "[RF] " fmt, ##args);  \
		}                                          \
	} while (0)

#else
#define RF_DBG(dm, comp, fmt, args...)                                         \
	do {                                                                   \
		struct dm_struct *__dm = dm;                                   \
		if ((comp) & __dm->rf_table.rf_dbg_comp) {                     \
			RT_TRACE(((struct rtl_priv *)__dm->adapter),           \
				 COMP_PHYDM, DBG_DMESG, "[RF] " fmt, ##args);  \
		}                                                              \
	} while (0)
#endif

#else /*#if DBG*/

#if (DM_ODM_SUPPORT_TYPE == ODM_WIN)
static __inline void RF_DBG(struct dm_struct *dm, int comp, char *fmt, ...)
{
}
#else
#define RF_DBG(dm, comp, fmt, args...)
#endif

#endif /*#if DBG*/

/*@============================================================*/
/*@ enumeration */
/*@============================================================*/

/*@============================================================*/
/*@ structure */
/*@============================================================*/

/*@============================================================*/
/*@ function prototype */
/*@============================================================*/

void halrf_cmd_parser(void *dm_void, char input[][16], u32 *_used, char *output,
		      u32 *_out_len, u32 input_num);

void halrf_init_debug_setting(void *dm_void);

#endif /*__HALRF_H__*/
