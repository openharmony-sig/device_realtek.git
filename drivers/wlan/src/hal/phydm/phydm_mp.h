/******************************************************************************
 * Copyright 2007 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#ifndef __PHYDM_MP_H__
#define __PHYDM_MP_H__

#define MP_VERSION "1.3"

/* @1 ============================================================
 * 1  Definition
 * 1 ============================================================
 */
/* @1 ============================================================
 * 1  structure
 * 1 ============================================================
 */
struct phydm_mp {
	/* @Rx OK count, statistics used in Mass Production Test.*/
	u64 tx_phy_ok_cnt;
	u64 rx_phy_ok_cnt;
	/* @Rx CRC32 error count, statistics used in Mass Production Test.*/
	u64 rx_phy_crc_err_cnt;
	/* @The Value of IO operation is depend of MptActType.*/
	u32 io_value;
	u32 rf_reg0;
	/* @u32 rfe_sel_a_0;*/
	/* @u32 rfe_sel_b_0;*/
	/* @u32 rfe_sel_c_0;*/
	/* @u32 rfe_sel_d_0;*/
	/* @u32 rfe_sel_a_1;*/
	/* @u32 rfe_sel_b_1;*/
	/* @u32 rfe_sel_c_1;*/
	/* @u32 rfe_sel_d_1;*/
};

/* @1 ============================================================
 * 1  enumeration
 * 1 ============================================================
 */
enum TX_MODE_OFDM {
	OFDM_OFF = 0,	
	OFDM_CONT_TX = 1,
	OFDM_SINGLE_CARRIER = 2,
	OFDM_SINGLE_TONE = 4,
};
/* @1 ============================================================
 * 1  function prototype
 * 1 ============================================================
 */
void phydm_mp_set_crystal_cap(void *dm_void, u8 crystal_cap);

void phydm_mp_set_single_tone(void *dm_void, boolean is_single_tone, u8 path);

void phydm_mp_set_carrier_supp(void *dm_void, boolean is_carrier_supp,
			       u32 rate_index);

void phydm_mp_set_single_carrier(void *dm_void, boolean is_single_carrier);

void phydm_mp_reset_rx_counters_phy(void *dm_void);

void phydm_mp_get_tx_ok(void *dm_void, u32 rate_index);

void phydm_mp_get_rx_ok(void *dm_void);
#endif
