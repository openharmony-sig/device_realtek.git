/******************************************************************************
 * Copyright 2007 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/
#ifndef __HAL_TXBF_8822B_H__
#define __HAL_TXBF_8822B_H__

#if (RTL8822B_SUPPORT == 1)
#ifdef PHYDM_BEAMFORMING_SUPPORT

void hal_txbf_8822b_enter(
	void *dm_void,
	u8 idx);

void hal_txbf_8822b_leave(
	void *dm_void,
	u8 idx);

void hal_txbf_8822b_status(
	void *dm_void,
	u8 beamform_idx);

void hal_txbf_8822b_config_gtab(
	void *dm_void);

void hal_txbf_8822b_fw_txbf(
	void *dm_void,
	u8 idx);
#else
#define hal_txbf_8822b_enter(dm_void, idx)
#define hal_txbf_8822b_leave(dm_void, idx)
#define hal_txbf_8822b_status(dm_void, idx)
#define hal_txbf_8822b_fw_txbf(dm_void, idx)
#define hal_txbf_8822b_config_gtab(dm_void)

#endif

#if (defined(CONFIG_BB_TXBF_API))
void phydm_8822btxbf_rfmode(void *dm_void, u8 su_bfee_cnt, u8 mu_bfee_cnt);

void phydm_8822b_sutxbfer_workaroud(void *dm_void, boolean enable_su_bfer,
				    u8 nc, u8 nr, u8 ng, u8 CB, u8 BW,
				    boolean is_vht);

#else
#define phydm_8822btxbf_rfmode(dm_void, su_bfee_cnt, mu_bfee_cnt)
#define phydm_8822b_sutxbfer_workaroud(dm_void, enable_su_bfer, nc, nr, ng, CB, BW, is_vht)
#endif

#else
#define hal_txbf_8822b_enter(dm_void, idx)
#define hal_txbf_8822b_leave(dm_void, idx)
#define hal_txbf_8822b_status(dm_void, idx)
#define hal_txbf_8822b_fw_txbf(dm_void, idx)
#define hal_txbf_8822b_config_gtab(dm_void)

#endif
#endif
