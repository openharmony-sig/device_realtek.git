/******************************************************************************
 * Copyright 2007 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/
#ifndef __HAL_TXBF_JAGUAR_H__
#define __HAL_TXBF_JAGUAR_H__
#if ((RTL8812A_SUPPORT == 1) || (RTL8821A_SUPPORT == 1))
#ifdef PHYDM_BEAMFORMING_SUPPORT

void hal_txbf_8812a_set_ndpa_rate(
	void *dm_void,
	u8 BW,
	u8 rate);

void hal_txbf_jaguar_enter(
	void *dm_void,
	u8 idx);

void hal_txbf_jaguar_leave(
	void *dm_void,
	u8 idx);

void hal_txbf_jaguar_status(
	void *dm_void,
	u8 idx);

void hal_txbf_jaguar_fw_txbf(
	void *dm_void,
	u8 idx);

void hal_txbf_jaguar_patch(
	void *dm_void,
	u8 operation);

void hal_txbf_jaguar_clk_8812a(
	void *dm_void);
#else

#define hal_txbf_8812a_set_ndpa_rate(dm_void, BW, rate)
#define hal_txbf_jaguar_enter(dm_void, idx)
#define hal_txbf_jaguar_leave(dm_void, idx)
#define hal_txbf_jaguar_status(dm_void, idx)
#define hal_txbf_jaguar_fw_txbf(dm_void, idx)
#define hal_txbf_jaguar_patch(dm_void, operation)
#define hal_txbf_jaguar_clk_8812a(dm_void)
#endif
#else

#define hal_txbf_8812a_set_ndpa_rate(dm_void, BW, rate)
#define hal_txbf_jaguar_enter(dm_void, idx)
#define hal_txbf_jaguar_leave(dm_void, idx)
#define hal_txbf_jaguar_status(dm_void, idx)
#define hal_txbf_jaguar_fw_txbf(dm_void, idx)
#define hal_txbf_jaguar_patch(dm_void, operation)
#define hal_txbf_jaguar_clk_8812a(dm_void)
#endif

#endif /*  @#ifndef __HAL_TXBF_JAGUAR_H__ */
