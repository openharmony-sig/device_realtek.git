/******************************************************************************
 * Copyright 2007 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/
#ifndef __HAL_COM_TXBF_H__
#define __HAL_COM_TXBF_H__

#if 0
typedef	bool
(*TXBF_GET)(
	void*			adapter,
	u8			get_type,
	void*			p_out_buf
	);

typedef	bool
(*TXBF_SET)(
	void*			adapter,
	u8			set_type,
	void*			p_in_buf
	);
#endif

enum txbf_set_type {
	TXBF_SET_SOUNDING_ENTER,
	TXBF_SET_SOUNDING_LEAVE,
	TXBF_SET_SOUNDING_RATE,
	TXBF_SET_SOUNDING_STATUS,
	TXBF_SET_SOUNDING_FW_NDPA,
	TXBF_SET_SOUNDING_CLK,
	TXBF_SET_TX_PATH_RESET,
	TXBF_SET_GET_TX_RATE
};

enum txbf_get_type {
	TXBF_GET_EXPLICIT_BEAMFORMEE,
	TXBF_GET_EXPLICIT_BEAMFORMER,
	TXBF_GET_MU_MIMO_STA,
	TXBF_GET_MU_MIMO_AP
};

/* @2 HAL TXBF related */
struct _HAL_TXBF_INFO {
	u8 txbf_idx;
	u8 ndpa_idx;
	u8 BW;
	u8 rate;

	struct phydm_timer_list txbf_fw_ndpa_timer;
#if (DM_ODM_SUPPORT_TYPE == ODM_WIN)
	RT_WORK_ITEM txbf_enter_work_item;
	RT_WORK_ITEM txbf_leave_work_item;
	RT_WORK_ITEM txbf_fw_ndpa_work_item;
	RT_WORK_ITEM txbf_clk_work_item;
	RT_WORK_ITEM txbf_status_work_item;
	RT_WORK_ITEM txbf_rate_work_item;
	RT_WORK_ITEM txbf_reset_tx_path_work_item;
	RT_WORK_ITEM txbf_get_tx_rate_work_item;
#endif
};

#ifdef PHYDM_BEAMFORMING_SUPPORT

void hal_com_txbf_beamform_init(
	void *dm_void);

void hal_com_txbf_config_gtab(
	void *dm_void);

void hal_com_txbf_enter_work_item_callback(
#if (DM_ODM_SUPPORT_TYPE == ODM_WIN)
	void *adapter
#else
	void *dm_void
#endif
	);

void hal_com_txbf_leave_work_item_callback(
#if (DM_ODM_SUPPORT_TYPE == ODM_WIN)
	void *adapter
#else
	void *dm_void
#endif
	);

void hal_com_txbf_fw_ndpa_work_item_callback(
#if (DM_ODM_SUPPORT_TYPE == ODM_WIN)
	void *adapter
#else
	void *dm_void
#endif
	);

void hal_com_txbf_clk_work_item_callback(
#if (DM_ODM_SUPPORT_TYPE == ODM_WIN)
	void *adapter
#else
	void *dm_void
#endif
	);

void hal_com_txbf_reset_tx_path_work_item_callback(
#if (DM_ODM_SUPPORT_TYPE == ODM_WIN)
	void *adapter
#else
	void *dm_void
#endif
	);

void hal_com_txbf_get_tx_rate_work_item_callback(
#if (DM_ODM_SUPPORT_TYPE == ODM_WIN)
	void *adapter
#else
	void *dm_void
#endif
	);

void hal_com_txbf_rate_work_item_callback(
#if (DM_ODM_SUPPORT_TYPE == ODM_WIN)
	void *adapter
#else
	void *dm_void
#endif
	);

void hal_com_txbf_fw_ndpa_timer_callback(
	struct phydm_timer_list *timer);

void hal_com_txbf_status_work_item_callback(
#if (DM_ODM_SUPPORT_TYPE == ODM_WIN)
	void *adapter
#else
	void *dm_void
#endif
	);

boolean
hal_com_txbf_set(
	void *dm_void,
	u8 set_type,
	void *p_in_buf);

boolean
hal_com_txbf_get(
	void *adapter,
	u8 get_type,
	void *p_out_buf);

#else
#define hal_com_txbf_beamform_init(dm_void) NULL
#define hal_com_txbf_config_gtab(dm_void) NULL
#define hal_com_txbf_enter_work_item_callback(_adapter) NULL
#define hal_com_txbf_leave_work_item_callback(_adapter) NULL
#define hal_com_txbf_fw_ndpa_work_item_callback(_adapter) NULL
#define hal_com_txbf_clk_work_item_callback(_adapter) NULL
#define hal_com_txbf_rate_work_item_callback(_adapter) NULL
#define hal_com_txbf_fw_ndpa_timer_callback(_adapter) NULL
#define hal_com_txbf_status_work_item_callback(_adapter) NULL
#define hal_com_txbf_get(_adapter, _get_type, _pout_buf)

#endif

#endif /*  @#ifndef __HAL_COM_TXBF_H__ */
