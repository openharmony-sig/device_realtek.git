/******************************************************************************
 * Copyright 2007 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/
#ifndef __HAL_TXBF_8814A_H__
#define __HAL_TXBF_8814A_H__

#if (RTL8814A_SUPPORT == 1)
#ifdef PHYDM_BEAMFORMING_SUPPORT

boolean
phydm_beamforming_set_iqgen_8814A(void *dm_void);

void hal_txbf_8814a_set_ndpa_rate(void *dm_void, u8 BW, u8 rate);

u8 hal_txbf_8814a_get_ntx(void *dm_void);

void hal_txbf_8814a_enter(void *dm_void, u8 idx);

void hal_txbf_8814a_leave(void *dm_void, u8 idx);

void hal_txbf_8814a_status(void *dm_void, u8 idx);

void hal_txbf_8814a_reset_tx_path(void *dm_void, u8 idx);

void hal_txbf_8814a_get_tx_rate(void *dm_void);

void hal_txbf_8814a_fw_txbf(void *dm_void, u8 idx);

#else

#define hal_txbf_8814a_set_ndpa_rate(dm_void, BW, rate)
#define hal_txbf_8814a_get_ntx(dm_void) 0
#define hal_txbf_8814a_enter(dm_void, idx)
#define hal_txbf_8814a_leave(dm_void, idx)
#define hal_txbf_8814a_status(dm_void, idx)
#define hal_txbf_8814a_reset_tx_path(dm_void, idx)
#define hal_txbf_8814a_get_tx_rate(dm_void)
#define hal_txbf_8814a_fw_txbf(dm_void, idx)
#define phydm_beamforming_set_iqgen_8814A(dm_void) 0

#endif

#else

#define hal_txbf_8814a_set_ndpa_rate(dm_void, BW, rate)
#define hal_txbf_8814a_get_ntx(dm_void) 0
#define hal_txbf_8814a_enter(dm_void, idx)
#define hal_txbf_8814a_leave(dm_void, idx)
#define hal_txbf_8814a_status(dm_void, idx)
#define hal_txbf_8814a_reset_tx_path(dm_void, idx)
#define hal_txbf_8814a_get_tx_rate(dm_void)
#define hal_txbf_8814a_fw_txbf(dm_void, idx)
#define phydm_beamforming_set_iqgen_8814A(dm_void) 0
#endif

#endif
