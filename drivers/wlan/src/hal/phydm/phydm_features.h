/******************************************************************************
 * Copyright 2007 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#ifndef __PHYDM_FEATURES_H__
#define __PHYDM_FEATURES_H__

#define CONFIG_RUN_IN_DRV
#define ODM_DC_CANCELLATION_SUPPORT		(ODM_RTL8188F | \
						 ODM_RTL8710B | \
						 ODM_RTL8192F | \
						 ODM_RTL8821C | \
						 ODM_RTL8822B | \
						 ODM_RTL8721D | \
						 ODM_RTL8710C)
#define ODM_RECEIVER_BLOCKING_SUPPORT	(ODM_RTL8188E | ODM_RTL8192E)

/*@20170103 YuChen add for FW API*/
#define PHYDM_FW_API_ENABLE_8822B		1
#define PHYDM_FW_API_FUNC_ENABLE_8822B		1
#define PHYDM_FW_API_ENABLE_8821C		1
#define PHYDM_FW_API_FUNC_ENABLE_8821C		1
#define PHYDM_FW_API_ENABLE_8195B		1
#define PHYDM_FW_API_FUNC_ENABLE_8195B		1
#define PHYDM_FW_API_ENABLE_8198F		1
#define PHYDM_FW_API_FUNC_ENABLE_8198F		1
#define PHYDM_FW_API_ENABLE_8822C 1
#define PHYDM_FW_API_FUNC_ENABLE_8822C 1
#define PHYDM_FW_API_ENABLE_8814B 1
#define PHYDM_FW_API_FUNC_ENABLE_8814B 1
#define PHYDM_FW_API_ENABLE_8812F 1
#define PHYDM_FW_API_FUNC_ENABLE_8812F 1
#define PHYDM_FW_API_ENABLE_8197G 1
#define PHYDM_FW_API_FUNC_ENABLE_8197G 1

#define CONFIG_POWERSAVING 0

#ifdef BEAMFORMING_SUPPORT
#if (BEAMFORMING_SUPPORT)
	#define PHYDM_BEAMFORMING_SUPPORT
#endif
#endif

#if (DM_ODM_SUPPORT_TYPE == ODM_WIN)
	#include	"phydm_features_win.h"
#elif (DM_ODM_SUPPORT_TYPE == ODM_CE)
	#include	"phydm_features_ce.h"
	/*@#include	"phydm_features_ce2_kernel.h"*/
#elif (DM_ODM_SUPPORT_TYPE == ODM_AP)
	#include	"phydm_features_ap.h"
#elif (DM_ODM_SUPPORT_TYPE == ODM_IOT)
	#include	"phydm_features_iot.h"
#endif

#endif
