/******************************************************************************
 * Copyright 2007 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#ifndef __PHYDMPSD_H__
#define __PHYDMPSD_H__

/*@#define PSD_VERSION	"1.0"*/ /*@2016.09.22  Dino*/
#define PSD_VERSION "1.1" /*@2016.10.07  Dino, Add Option for PSD Tone index Selection */

#ifdef CONFIG_PSD_TOOL


struct psd_info {
	u8	psd_in_progress;
	u32	psd_reg;
	u32	psd_report_reg;
	u8	psd_pwr_common_offset;
	u16	sw_avg_time;
	u16	fft_smp_point;
	u32	rf_0x18_bkp;
	u32	rf_0x18_bkp_b;
	u16	psd_fc_channel;
	u32	psd_bw_rf_reg;
	u8	psd_result[128];
	u8	noise_k_en;
};

u32 phydm_get_psd_data(void *dm_void, u32 psd_tone_idx, u32 igi);

void phydm_psd_debug(void *dm_void, char input[][16], u32 *_used,
		     char *output, u32 *_out_len);

u8 phydm_psd(void *dm_void, u32 igi, u16 start_point, u16 stop_point);

void phydm_psd_para_setting(void *dm_void, u8 sw_avg_time, u8 hw_avg_time,
			    u8 i_q_setting, u16 fft_smp_point, u8 ant_sel,
			    u8 psd_input, u8 channel, u8 noise_k_en);

void phydm_psd_init(void *dm_void);

u8 phydm_get_psd_result_table(void *dm_void, int index);

#endif
#endif
