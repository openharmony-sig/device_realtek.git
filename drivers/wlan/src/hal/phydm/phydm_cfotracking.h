/******************************************************************************
 * Copyright 2007 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#ifndef __PHYDMCFOTRACK_H__
#define __PHYDMCFOTRACK_H__

/* 2019.03.28 fix 8197G crystal_cap register address*/
#define CFO_TRACKING_VERSION "2.4"

#define		CFO_TRK_ENABLE_TH	20 /* @kHz enable CFO_Track threshold*/
#define		CFO_TRK_STOP_TH		10 /* @kHz disable CFO_Track threshold*/
#define		CFO_TH_ATC		80 /* @kHz */

struct phydm_cfo_track_struct {
	boolean		is_atc_status;
	boolean		is_adjust;	/*@already modify crystal cap*/
	u8		crystal_cap;
	u8		crystal_cap_default;
	u8		def_x_cap;
	s32		CFO_tail[4];
	u32		CFO_cnt[4];
	s32		CFO_ave_pre;
	u32		packet_count;
	u32		packet_count_pre;
};

struct phydm_cfo_rpt {
	s32 cfo_rpt_s[PHYDM_MAX_RF_PATH];
	s32 cfo_rpt_l[PHYDM_MAX_RF_PATH];
	s32 cfo_rpt_acq[PHYDM_MAX_RF_PATH];
	s32 cfo_rpt_sec[PHYDM_MAX_RF_PATH];
	s32 cfo_rpt_end[PHYDM_MAX_RF_PATH];
};

void phydm_get_cfo_info(void *dm_void, struct phydm_cfo_rpt *cfo);

boolean phydm_set_crystal_cap_reg(void *dm_void, u8 crystal_cap);

void phydm_set_crystal_cap(void *dm_void, u8 crystal_cap);

void phydm_cfo_tracking_init(void *dm_void);

void phydm_cfo_tracking(void *dm_void);

void phydm_parsing_cfo(void *dm_void, void *pktinfo_void, s8 *pcfotail,
		       u8 num_ss);
void phydm_cfo_tracking_debug(void *dm_void, char input[][16], u32 *_used,
			      char *output, u32 *_out_len);
#if (DM_ODM_SUPPORT_TYPE == ODM_WIN)
void phy_Init_crystal_capacity(void *dm_void, u8 crystal_cap);
#endif
#endif
