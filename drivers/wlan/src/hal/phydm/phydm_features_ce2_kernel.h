/******************************************************************************
 * Copyright 2007 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#ifndef __PHYDM_FEATURES_CE_H__
#define __PHYDM_FEATURES_CE_H__

#define PHYDM_LA_MODE_SUPPORT			0

#if (RTL8822B_SUPPORT || RTL8812A_SUPPORT || RTL8197F_SUPPORT ||\
	RTL8192F_SUPPORT)
	#define DYN_ANT_WEIGHTING_SUPPORT
#endif

#if (RTL8822B_SUPPORT || RTL8821C_SUPPORT)
	#define FAHM_SUPPORT
#endif
	#define NHM_SUPPORT
	#define CLM_SUPPORT

#if (RTL8822B_SUPPORT)
	#define PHYDM_TXA_CALIBRATION
#endif

#if (RTL8188F_SUPPORT || RTL8710B_SUPPORT || RTL8821C_SUPPORT ||\
	RTL8822B_SUPPORT || RTL8192F_SUPPORT)
	#define	PHYDM_DC_CANCELLATION
#endif

#if (RTL8192F_SUPPORT == 1)
	/*#define	CONFIG_8912F_SPUR_CALIBRATION*/
#endif

#if (RTL8822B_SUPPORT == 1)
	/* #define	CONFIG_8822B_SPUR_CALIBRATION */
#endif

#define PHYDM_SUPPORT_CCKPD
#define PHYDM_SUPPORT_ADAPTIVITY

#ifdef CONFIG_DFS_MASTER
	#define CONFIG_PHYDM_DFS_MASTER
#endif

#define	CONFIG_BB_TXBF_API
#define	CONFIG_PHYDM_DEBUG_FUNCTION

#ifdef CONFIG_BT_COEXIST
	#define	ODM_CONFIG_BT_COEXIST
#endif
#define	PHYDM_SUPPORT_RSSI_MONITOR
#define CFG_DIG_DAMPING_CHK


#ifdef PHYDM_BEAMFORMING_SUPPORT
	#if (RTL8192F_SUPPORT || RTL8195B_SUPPORT || RTL8821C_SUPPORT ||\
	     RTL8822B_SUPPORT || RTL8197F_SUPPORT || RTL8198F_SUPPORT ||\
	     RTL8822C_SUPPORT || RTL8814B_SUPPORT)
		#define	DRIVER_BEAMFORMING_VERSION2
	#endif
#endif

#endif
