/******************************************************************************
 * Copyright 2007 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#ifndef __PHYDMANTDECT_H__
#define __PHYDMANTDECT_H__

#define ANTDECT_VERSION "2.1"

#if (defined(CONFIG_ANT_DETECTION))
/* @#if( DM_ODM_SUPPORT_TYPE & (ODM_WIN |ODM_CE)) */
/* @ANT Test */
#define ANTTESTALL 0x00 /*@ant A or B will be Testing*/
#define ANTTESTA 0x01 /*@ant A will be Testing*/
#define ANTTESTB 0x02 /*@ant B will be testing*/

#define MAX_ANTENNA_DETECTION_CNT 10

struct _ANT_DETECTED_INFO {
	boolean is_ant_detected;
	u32 db_for_ant_a;
	u32 db_for_ant_b;
	u32 db_for_ant_o;
};

enum dm_swas {
	antenna_a = 1,
	antenna_b = 2,
	antenna_max = 3,
};

/* @1 [1. Single Tone method] =================================================== */

void odm_single_dual_antenna_default_setting(
	void *dm_void);

boolean
odm_single_dual_antenna_detection(
	void *dm_void,
	u8 mode);

/* @1 [2. Scan AP RSSI method] ================================================== */

#define sw_ant_div_check_before_link odm_sw_ant_div_check_before_link

boolean
odm_sw_ant_div_check_before_link(
	void *dm_void);

/* @1 [3. PSD method] ========================================================== */

void odm_single_dual_antenna_detection_psd(
	void *dm_void);

void odm_sw_ant_detect_init(void *dm_void);
#endif
#endif
