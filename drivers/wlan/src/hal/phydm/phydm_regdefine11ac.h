/******************************************************************************
 * Copyright 2007 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#ifndef __ODM_REGDEFINE11AC_H__
#define __ODM_REGDEFINE11AC_H__

/* @2 RF REG LIST */



/* @2 BB REG LIST */
/* PAGE 8 */
#define	ODM_REG_CCK_RPT_FORMAT_11AC		0x804
#define	ODM_REG_BB_RX_PATH_11AC			0x808
#define	ODM_REG_BB_TX_PATH_11AC			0x80c
#define	ODM_REG_BB_ATC_11AC			0x860
#define	ODM_REG_EDCCA_POWER_CAL			0x8dc
#define	ODM_REG_DBG_RPT_11AC			0x8fc
/* PAGE 9 */
#define	ODM_REG_EDCCA_DOWN_OPT			0x900
#define	ODM_REG_ACBB_EDCCA_ENHANCE		0x944
#define	odm_adc_trigger_jaguar2			0x95C	/*@ADC sample mode*/
#define	ODM_REG_OFDM_FA_RST_11AC		0x9A4
#define	ODM_REG_CCX_PERIOD_11AC			0x990
#define	ODM_REG_NHM_TH9_TH10_11AC		0x994
#define	ODM_REG_CLM_11AC			0x994
#define	ODM_REG_NHM_TH3_TO_TH0_11AC		0x998
#define	ODM_REG_NHM_TH7_TO_TH4_11AC		0x99c
#define	ODM_REG_NHM_TH8_11AC			0x9a0
#define	ODM_REG_NHM_9E8_11AC			0x9e8
#define	ODM_REG_CSI_CONTENT_VALUE		0x9b4
/* PAGE A */
#define	ODM_REG_CCK_CCA_11AC			0xA0A
#define	ODM_REG_CCK_FA_RST_11AC			0xA2C
#define	ODM_REG_CCK_FA_11AC			0xA5C
/* PAGE B */
#define	ODM_REG_RST_RPT_11AC			0xB58
/* PAGE C */
#define	ODM_REG_TRMUX_11AC			0xC08
#define	ODM_REG_IGI_A_11AC			0xC50
/* PAGE E */
#define	ODM_REG_IGI_B_11AC			0xE50
#define	ODM_REG_ANT_11AC_B			0xE08
/* PAGE F */
#define	ODM_REG_CCK_CRC32_CNT_11AC		0xF04
#define	ODM_REG_CCK_CCA_CNT_11AC		0xF08
#define	ODM_REG_VHT_CRC32_CNT_11AC		0xF0c
#define	ODM_REG_HT_CRC32_CNT_11AC		0xF10
#define	ODM_REG_OFDM_CRC32_CNT_11AC		0xF14
#define	ODM_REG_OFDM_FA_11AC			0xF48
#define	ODM_REG_OFDM_FA_TYPE1_11AC		0xFCC
#define	ODM_REG_OFDM_FA_TYPE2_11AC		0xFD0
#define	ODM_REG_OFDM_FA_TYPE3_11AC		0xFBC
#define	ODM_REG_OFDM_FA_TYPE4_11AC		0xFC0
#define	ODM_REG_OFDM_FA_TYPE5_11AC		0xFC4
#define	ODM_REG_OFDM_FA_TYPE6_11AC		0xFC8
#define	ODM_REG_RPT_11AC			0xfa0
#define	ODM_REG_CLM_RESULT_11AC			0xfa4
#define	ODM_REG_NHM_CNT_11AC			0xfa8
#define ODM_REG_NHM_DUR_READY_11AC		0xfb4

#define	ODM_REG_NHM_CNT7_TO_CNT4_11AC		0xfac
#define	ODM_REG_NHM_CNT11_TO_CNT8_11AC		0xfb0
/* PAGE 18 */
#define	ODM_REG_IGI_C_11AC			0x1850
/* PAGE 1A */
#define	ODM_REG_IGI_D_11AC			0x1A50

/* PAGE 1D */
#define	ODM_REG_IGI_11AC3			0x1D70

/* @2 MAC REG LIST */
#define	ODM_REG_RESP_TX_11AC			0x6D8



/* @DIG Related */
#define	ODM_BIT_IGI_11AC			0x0000007F
#define	ODM_BIT_IGI_B_11AC3			0x00007F00
#define	ODM_BIT_IGI_C_11AC3			0x007F0000
#define	ODM_BIT_IGI_D_11AC3			0x7F000000
#define	ODM_BIT_CCK_RPT_FORMAT_11AC		BIT(16)
#define	ODM_BIT_BB_RX_PATH_11AC			0xF
#define	ODM_BIT_BB_TX_PATH_11AC			0xF
#define	ODM_BIT_BB_ATC_11AC			BIT(14)

#endif
