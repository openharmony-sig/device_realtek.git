/******************************************************************************
 * Copyright 2007 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#ifndef __PHYDM_CCK_RX_PATHDIV_H__
#define __PHYDM_CCK_RX_PATHDIV_H__

#define CCK_RX_PATHDIV_VERSION "1.1"

/* @1 ============================================================
 * 1  Definition
 * 1 ============================================================
 */
/* @1 ============================================================
 * 1  structure
 * 1 ============================================================
 */
struct phydm_cck_rx_pathdiv {
	boolean en_cck_rx_pathdiv;
	u32	path_a_sum;
	u32	path_b_sum;
	u16	path_a_cnt;
	u16	path_b_cnt;
	u8	rssi_fa_th;
	u8	rssi_th;
};

/* @1 ============================================================
 * 1  enumeration
 * 1 ============================================================
 */

/* @1 ============================================================
 * 1  function prototype
 * 1 ============================================================
 */
void phydm_cck_rx_pathdiv_watchdog(void *dm_void);

void phydm_cck_rx_pathdiv_init(void *dm_void);

void phydm_process_rssi_for_cck_rx_pathdiv(void *dm_void, void *phy_info_void,
					   void *pkt_info_void);

void phydm_cck_rx_pathdiv_dbg(void *dm_void, char input[][16], u32 *_used,
			      char *output, u32 *_out_len);
#endif
