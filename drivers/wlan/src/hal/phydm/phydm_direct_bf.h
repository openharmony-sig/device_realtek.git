/******************************************************************************
 * Copyright 2007 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#ifndef __PHYDM_DIR_BF_H__
#define __PHYDM_DIR_BF_H__

#ifdef CONFIG_DIRECTIONAL_BF
#define ANGLE_NUM	12

/*@
 * ============================================================
 * function prototype
 * ============================================================
 */
void phydm_iq_gen_en(void *dm_void);
void phydm_dis_cdd(void *dm_void);
void phydm_pathb_q_matrix_rotate_en(void *dm_void);
void phydm_pathb_q_matrix_rotate(void *dm_void, u16 idx);
void phydm_set_direct_bfer(void *dm_void, u16 phs_idx, u8 su_idx);
void phydm_set_direct_bfer_txdesc_en(void *dm_void, u8 enable);
#endif
#endif
