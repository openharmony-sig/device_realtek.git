/******************************************************************************
 * Copyright 2007 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#ifndef __HALHWOUTSRC_H__
#define __HALHWOUTSRC_H__

/*@--------------------------Define -------------------------------------------*/
#define AGC_DIFF_CONFIG_MP(ic, band)				\
	(odm_read_and_config_mp_##ic##_agc_tab_diff(dm,		\
	array_mp_##ic##_agc_tab_diff_##band,			\
	sizeof(array_mp_##ic##_agc_tab_diff_##band) / sizeof(u32)))
#define AGC_DIFF_CONFIG_TC(ic, band)				\
	(odm_read_and_config_tc_##ic##_agc_tab_diff(dm,		\
	array_tc_##ic##_agc_tab_diff_##band,			\
	sizeof(array_tc_##ic##_agc_tab_diff_##band) / sizeof(u32)))
#if defined(DM_ODM_CE_MAC80211)
#else
#define AGC_DIFF_CONFIG(ic, band)                     \
	do {                                          \
		if (dm->is_mp_chip)                   \
			AGC_DIFF_CONFIG_MP(ic, band); \
		else                                  \
			AGC_DIFF_CONFIG_TC(ic, band); \
	} while (0)
#endif
/*@************************************************************
 * structure and define
 ************************************************************/

enum hal_status
odm_config_rf_with_tx_pwr_track_header_file(struct dm_struct *dm);

enum hal_status
odm_config_rf_with_header_file(struct dm_struct *dm,
			       enum odm_rf_config_type config_type,
			       u8 e_rf_path);

enum hal_status
odm_config_bb_with_header_file(struct dm_struct *dm,
			       enum odm_bb_config_type config_type);

enum hal_status
odm_config_mac_with_header_file(struct dm_struct *dm);

u32 odm_get_hw_img_version(struct dm_struct *dm);

u32 query_phydm_trx_capability(struct dm_struct *dm);

u32 query_phydm_stbc_capability(struct dm_struct *dm);

u32 query_phydm_ldpc_capability(struct dm_struct *dm);

u32 query_phydm_txbf_parameters(struct dm_struct *dm);

u32 query_phydm_txbf_capability(struct dm_struct *dm);

#endif /*@#ifndef	__HALHWOUTSRC_H__*/
