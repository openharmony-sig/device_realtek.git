/******************************************************************************
 * Copyright 2007 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/
#ifndef __BTC_BASIC_TYPES_H__
#define __BTC_BASIC_TYPES_H__

#define IN
#define OUT
#define VOID void
typedef void *PVOID;

#define u1Byte		u8
#define pu1Byte		u8*

#define u2Byte		u16
#define pu2Byte		u16*

#define u4Byte		u32
#define pu4Byte		u32*

#define u8Byte		u64
#define pu8Byte		u64*

#define s1Byte		s8
#define ps1Byte		s8*

#define s2Byte		s16
#define ps2Byte		s16*

#define s4Byte		s32
#define ps4Byte		s32*

#define s8Byte		s64
#define ps8Byte		s64*

#define UCHAR u8
#define USHORT u16
#define UINT u32
#define ULONG u32
#define PULONG u32*

#endif /* __BTC_BASIC_TYPES_H__ */
