/******************************************************************************
 * Copyright 2016 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/
#ifndef _RTL8821CS_H_
#define _RTL8821CS_H_

#include <drv_types.h>		/* PADAPTER */

/* rtl8821cs_halinit.c */
u32 rtl8821cs_hal_init(PADAPTER);
u32 rtl8821cs_hal_deinit(PADAPTER adapter);

void rtl8821cs_init_default_value(PADAPTER);

/* rtl8821cs_ops.c */
u32 rtl8821cs_get_interrupt(PADAPTER);
void rtl8821cs_clear_interrupt(PADAPTER, u32 hisr);
u32 rtl8821cs_get_himr(PADAPTER adapter);
void rtl8821cs_update_himr(PADAPTER adapter, u32 himr);
void rtl8821cs_update_interrupt_mask(PADAPTER padapter, u32 AddMSR, u32 RemoveMSR);

/* rtl8821cs_xmit.c */


#endif /* _RTL8821CS_HAL_H_ */
