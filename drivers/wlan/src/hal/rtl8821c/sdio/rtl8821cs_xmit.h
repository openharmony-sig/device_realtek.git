/******************************************************************************
 * Copyright 2016 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/
#ifndef _RTL8821CS_XMIT_H_
#define _RTL8821CS_XMIT_H_

#include <drv_types.h>		/* PADAPTER, rtw_xmit.h and etc. */

s32 rtl8821cs_init_xmit_priv(PADAPTER);
void rtl8821cs_free_xmit_priv(PADAPTER);
s32 rtl8821cs_hal_xmit_enqueue(PADAPTER, struct xmit_frame *);
s32 rtl8821cs_hal_xmit(PADAPTER, struct xmit_frame *);
s32 rtl8821cs_mgnt_xmit(PADAPTER, struct xmit_frame *);

s32 rtl8821cs_xmit_buf_handler(PADAPTER);
thread_return rtl8821cs_xmit_thread(thread_context);

#endif /* _RTL8821CS_XMIT_H_ */
