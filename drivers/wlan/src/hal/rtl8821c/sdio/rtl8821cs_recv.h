/******************************************************************************
 * Copyright 2016 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/
#ifndef _RTL8821CS_RECV_H_
#define _RTL8821CS_RECV_H_

#include <drv_types.h>		/* PADAPTER and etc. */

s32 rtl8821cs_init_recv_priv(PADAPTER);
void rtl8821cs_free_recv_priv(PADAPTER);
/*_pkt* rtl8821cs_alloc_recvbuf_skb(struct recv_buf*, u32 size);*/
/*void rtl8821cs_free_recvbuf_skb(struct recv_buf*);*/
void rtl8821cs_rxhandler(PADAPTER, struct recv_buf *);
s32 rtl8821cs_recv_hdl(_adapter *adapter);

#endif /* _RTL8821CS_RECV_H_ */
