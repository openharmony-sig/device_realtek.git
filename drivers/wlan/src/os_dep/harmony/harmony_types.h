/******************************************************************************
 * Copyright 2007 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/
#ifndef __OSDEP_HARMONY_TYPE_H_
#define __OSDEP_HARMONY_TYPE_H_

//#include "los_typedef.h"
#include "osal.h"
#include "osal/osal_atomic.h"

//#include "osdep_service.h"
//#include "basic_types.h"
//#include <rtw_rf.h>
#include "errno.h"
//#include <asm/atomic.h>
#ifdef CONFIG_USB_HCI
#include "linux_usb.h"
#endif
//#include <usb.h>
//#include <usbdi.h>

//#include "linux/version.h"
//#include <drv_conf.h>
//#include <ieee80211.h>
//#include <wlan_bssdef.h>
//#include <wifi.h>
#include <netinet/ip.h>
//#include <if_ether.h>

#include "linux/workqueue.h"

#ifdef CONFIG_USB_HCI
#include "implementation/global_implementation.h"
#endif
#include "linux/slab.h"

#include "wireless.h"
#include "autoconf.h"
#include "net_device.h"

/************basic define**************/
#ifndef s8
	typedef signed char s8;
#endif
#ifndef u8
	typedef unsigned char u8;
#endif
#ifndef s16
	typedef signed short s16;
#endif
#ifndef u16
	typedef unsigned short u16;
#endif
#ifndef s32
	typedef signed int s32;
#endif
#ifndef u32
	typedef unsigned int u32;
#endif
#ifndef uint	
	typedef unsigned int	uint;
#endif
#ifndef sint
	typedef	signed int		sint;
#endif
#ifndef s64
	typedef signed long long s64;
#endif
#ifndef u64
	typedef unsigned long long u64;
#endif



typedef u16 __le16;
typedef u32 __le32;
typedef u16 __be16;
typedef u32 __be32;


/***********advanced define**************/
#ifdef CONFIG_USB_HCI
	typedef struct urb *  PURB;
#endif

//for compat with linux
#define __must_check 
#define KERNEL_VERSION(a,b,c) (((a) << 16) + ((b) << 8) + (c))
#define LINUX_VERSION_CODE KERNEL_VERSION(3, 5, 0)


typedef OsalAtomic ATOMIC_T;
typedef struct OsalMutex _mutex;
typedef unsigned long _irqL;
typedef struct work_struct _workitem;
#if (LOSCFG_KERNEL_SMP ==1)
typedef	OsalSpinlock _lock;
#else
typedef	struct OsalMutex _lock;
#endif
typedef	struct OsalSem  _sema;
typedef struct DListHead	_list;
struct	__queue	{
	struct	DListHead queue;
	_lock	lock;
};
typedef struct	__queue	_queue;
struct __timer{
	OsalTimer osaltimer;
	void (*function)(void *);
	unsigned long arg;
	int create;
};
typedef struct __timer _timer;
typedef unsigned char	BOOLEAN,*PBOOLEAN;
typedef void*	_xqueue;//not used,just for define


//----- ------------------------------------------------------------------
// Device structure
//----- ------------------------------------------------------------------
/****************skb define****************/

struct sk_buff_head {
	/* These two members must be first. */
	struct sk_buff	*next;
	struct sk_buff	*prev;

	u32		qlen;
	_lock	lock;
};

struct sk_buff {
	/* These two members must be first. */
	struct sk_buff		*next;		/* Next buffer in list */
	struct sk_buff		*prev;		/* Previous buffer in list */
	
	struct sk_buff_head	*list;		/* List we are on */	
	unsigned char		*head;		/* Head of buffer */
	unsigned char		*data;		/* Data head pointer */
	unsigned char		*tail;		/* Tail pointer	*/
	unsigned char		*end;		/* End pointer */ 
	void	*dev;		/* Device we arrived on/are leaving by */	
	unsigned int 		len;		/* Length of actual data */	
#ifdef LITOS_TX_ZERO_COPY
	struct pbuf_dma_info *dma_info;
#endif
	unsigned char zerocp_flag;
	unsigned char alloc_buf_flag;

#ifdef CONFIG_DONT_CARE_TP
	int 			dyalloc_flag;
#endif
	u32			priority; //for compat with linux
	unsigned char     mac_header;
};

struct skb_data {
	//struct list_head list;
	ATOMIC_T ref;
	unsigned char buf[0];
	
};

struct rtw_netdev_priv_indicator {
	void *priv;
	u32 sizeof_priv;
};
/*========================*/

typedef struct NetDevStats net_device_stats;

struct net_device {
	char			name[16];
	char 			type;
	void			*priv;		/* pointer to private data */
	unsigned char	dev_addr[6];	/* set during bootup */
	struct NetDevice *dev;
};

typedef struct {
	struct net_device *dev;		/* Binding wlan driver netdev */
	void *skb;			/* pending Rx packet */
	unsigned int tx_busy;
	unsigned int rx_busy;
	unsigned char enable;
	unsigned char mac[6];
} Rltk_wlan_t;


#define netdev_priv(dev)		dev->priv

typedef	NetBuf	_nic_pkt;

typedef	struct sk_buff	_pkt;
typedef unsigned char	_buffer;

typedef struct	__queue	_queue;
typedef struct DListHead	_list;
typedef	int	_OS_STATUS;
//typedef u32	_irqL;
typedef unsigned long _irqL;
typedef	struct	net_device * _nic_hdl;

typedef u32		_thread_hdl_;
typedef u32		thread_return;
typedef void*	thread_context;

typedef void (*timer_handler)(unsigned long);
//typedef TSK_INIT_PARAM_S _rtw_task_info_s;
typedef struct OsalThread _rtw_task_info_s;//for save task id
//typedef void timer_hdl_return;
//typedef void* timer_hdl_context;

#ifndef CONFIG_SDIO_HCI
typedef u32 dma_addr_t;
#endif 

typedef u32	SIZE_T;
#define IN
#define OUT
#define VOID void
#define NDIS_OID unsigned int
#define NDIS_STATUS unsigned int
typedef void * PVOID;

struct iw_request_info { 
	u16           cmd;            /* Wireless Extension command */
	u16           flags;          /* More to come ;-) */ 
};
#if (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,24))
	#define DMA_BIT_MASK(n) (((n) == 64) ? ~0ULL : ((1ULL<<(n))-1))
#endif

#define LIST_CONTAINOR(ptr, type, member) \
        ((type *)((char *)(ptr)-(SIZE_T)(&((type *)0)->member)))
#ifndef container_of
#define container_of(ptr, type, member) ({	\
		const typeof( ((type *)0)->member ) *__mptr = (ptr); \
		(type *)( (char *)__mptr - offsetof(type,member) );})
#endif
		
#define FIELD_OFFSET(s,field)	((int)&((s*)(0))->field)

#define RTW_TIMER_HDL_ARGS void *FunctionContext
// limitation of path length
#define PATH_LENGTH_MAX PATH_MAX

#define rtw_netdev_priv(netdev) ( ((struct rtw_netdev_priv_indicator *)netdev_priv(netdev))->priv )
#define rtw_get_netdev(dev) rtw_netdev[0]

#define NDEV_FMT "%s"
#define NDEV_ARG(ndev) ndev->name
#define ADPT_FMT "%s"
#define ADPT_ARG(adapter) adapter->pnetdev->name
#define FUNC_NDEV_FMT "%s(%s)"
#define FUNC_NDEV_ARG(ndev) __func__, ndev->name
#define FUNC_ADPT_FMT "%s(%s)"
#define FUNC_ADPT_ARG(adapter) __func__, adapter->pnetdev->name

struct net_device *rtw_alloc_etherdev_with_old_priv(int sizeof_priv, void *old_priv);
extern struct net_device * rtw_alloc_etherdev(int sizeof_priv);

#define STRUCT_PACKED __attribute__ ((packed))

#endif
