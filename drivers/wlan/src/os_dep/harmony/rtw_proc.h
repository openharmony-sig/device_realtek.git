/******************************************************************************
 * Copyright 2007 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/
 
#ifndef __RTW_PROC_H__
#define __RTW_PROC_H__

#ifdef CONFIG_PROC_DEBUG
#include <linux/proc_fs.h>
#include <linux/seq_file.h>

struct rtw_proc_hdl {
	char *name;
	int (*show)(struct seq_file *, void *);
	ssize_t (*write)(struct file *file, const char __user *buffer, size_t count, loff_t *pos, void *data);
};

//#ifdef CONFIG_PROC_DEBUG

struct proc_dir_entry *get_rtw_drv_proc(void);
int rtw_drv_proc_init(void);
void rtw_drv_proc_deinit(void);
struct proc_dir_entry *rtw_adapter_proc_init(struct net_device *dev);
void rtw_adapter_proc_deinit(struct net_device *dev);
void rtw_adapter_proc_replace(struct net_device *dev);

#else //!CONFIG_PROC_DEBUG

#define get_rtw_drv_proc() NULL
#define rtw_drv_proc_init() 0
#define rtw_drv_proc_deinit() do {} while (0)
#define rtw_adapter_proc_init(dev) NULL
#define rtw_adapter_proc_deinit(dev) do {} while (0)
#define rtw_adapter_proc_replace(dev) do {} while (0)

#endif //!CONFIG_PROC_DEBUG

#endif //__RTW_PROC_H__
