/******************************************************************************
 * Copyright 2007 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/
#ifndef __WLAN_CONFIG_H_
#define __WLAN_CONFIG_H_

#include "los_typedef.h"
/**************************************************************************/

/*
 * Wireless Configuration commands are sent to the driver as driver direct
 * commands. As per NetX Reference Manual the command can be any number after
 * NX_LINK_USER_COMMAND
 * - Change this number if your system uses the same number with WLAN_COMMAND
 */


#define WC_MAC_ADDR_LEN     (6)
#define WC_MAX_RATES        (16)    /* Max no. of rates supported */

#define WC_STATUS_SUCCESS       0
#define WC_STATUS_ERROR         -1

#define WC_CONNECTED            1   /* Connected to an Access Point / Adhoc Network */
#define WC_DISCONNECTED         2   /* Disconnected from an Access Point / Adhoc Network */
#define WC_SCAN_RESULTS         3
#define WC_ADHOC_STARTED        4   /* Started an Adhoc Network */
#define WC_ADHOC_JOINED         5   /* Joined to Adhoc Network */
#define WC_ADHOC_CONNECTED      6   /* Station Connected to our Adhoc Network */
#define WC_ADHOC_DISCONNECTED   7   /* Station Disconnected from our Adhoc Network */
#define WC_DRIVER_READY         8   /* MOAL and MLAN modules ready to communicate with WLAN module */
#define WC_DRIVER_NOT_READY     9   /* MOAL and MLAN modules Not ready to communicate with WLAN module */
#define WC_CARD_REMOVED         10   /* WLAN module removed */
#define WC_UAP_STARTED          11   
#define WC_UAP_STA_CONNECTED    12
#define WC_UAP_STA_DISCONNECTED 13   

#define WC_CLEAR_IE             0
#define WC_SET_IE               1

/** Action GET */
#define WC_ACTION_GET                       (0)
/** Action SET */
#define WC_ACTION_SET                       (1)




#define WC_ENABLE                           (1)
#define WC_DISABLE                          (0)

struct rtw_wc_param {
    void *wc_apps_data;
	void *wc_apps_data1;
    void *wc_apps_cb;
    unsigned int param1;  
    unsigned int param2;     
    unsigned int param3;
    unsigned int param4; 
};

struct rtw_wc_command {
    unsigned int wc_cmd;       /* iwconfig cmd no. */
    struct rtw_wc_param iwcp;   /* param for iwconfig cmd */
};

/* iwconf cmd flags */
#define WC_FLAG_SET_CHANNEL         0x01    /* Set Using Channel number */
#define WC_FLAG_SET_FREQ            0x02    /* Set Using Frequency(MHz) */

#define WC_FLAG_TXPOW_AUTO          0x01    /* Tx Power Auto */
#define WC_FLAG_TXPOW_FIXED         0x02    /* Tx Power Fixed */
#define WC_FLAG_RADIO_OFF           0x04    /* Radio Off */
#define WC_FLAG_RADIO_ON            0x08    /* Radio On */

#define WC_FLAG_PS_DISABLE          0x01    /* Disable PS mode */
#define WC_FLAG_PS_ENABLE           0x02    /* Enable PS mode */

#define WC_FLAG_ENCODE_ENABLED      0x00
#define WC_FLAG_ENCODE_NOKEY        0x01
#define WC_FLAG_ENCODE_DISABLED     0x02
#define WC_FLAG_ENCODE_OPEN         0x04
#define WC_FLAG_ENCODE_RESTRICTED   0x08
#define WC_FLAG_ENCODE_TXKEY        0x10
#define WC_FLAG_ENCODE_WPA          0x20
#define WC_FLAG_ENCODE_RSN          0x40

#define WC_ESSID_MAX_SIZE           32      /* Max size of essid */
#define WC_MAX_ESSID_LEN            (WC_ESSID_MAX_SIZE + 1)  /* (+1) for '\0' termination */

#define WC_WPA_ESSID_LEN_MASK       (0x000000FF)
#define WC_WPA_ESSID_SHIFT          (0)
#define WC_WPA_PSK_LEN_MASK         (0x0000FF00)
#define WC_WPA_PSK_SHIFT            (8)
#define WC_WPA_PASSPHRASE_LEN_MASK  (0x00FF0000)
#define WC_WPA_PASSPHRASE_SHIFT     (16)

#define WC_WPA_ACTION_MASK          (0x0F)
#define WC_WPA_ESSID_PRESENT        (1 << 0)
#define WC_WPA_BSSID_PRESENT        (1 << 1)
#define WC_WPA_PSK_PRESENT          (1 << 2)
#define WC_WPA_PASSPHRASE_PRESENT   (1 << 3)
#define WC_WPA_UCIPHER_TKIP         (1 << 4)
#define WC_WPA_UCIPHER_AES          (1 << 5)
#define WC_WPA_MCIPHER_TKIP         (1 << 6)
#define WC_WPA_MCIPHER_AES          (1 << 7)
#define WC_WPA_VERSION_WPA          (1 << 8)
#define WC_WPA_VERSION_WPA2         (1 << 9)


#define WC_HTCAP_MASK               (0x7F)
#define WC_FLAG_HTCAP_ENABLE        (1 << 0)
#define WC_FLAG_HTCAP_40M           (1 << 1)
#define WC_FLAG_HTCAP_SHORTGI_20M   (1 << 2)
#define WC_FLAG_HTCAP_SHORTGI_40M   (1 << 3)
#define WC_FLAG_HTCAP_INTOL_40M     (1 << 4)
#define WC_FLAG_HTCAP_GREEN_FIELD   (1 << 5)
#define WC_FLAG_HTCAP_RX_STBC       (1 << 6)



/*
 * Bit 1: 20/40 Mhz Enable/Disable
 * Bit 4: Green Filed Enable/Disable
 * Bit 5: Short GI in 20 Mhz Enable/Disable
 * Bit 6: Short GI in 40 Mhz Enable/Disable
 *
 * Defualt Value is set to 0x20
 */

#define WC_FLAG_HTTXCFG_20_40M      (1 << 1)
#define WC_FLAG_HTTXCFG_GREEN_FIELD (1 << 4) 
#define WC_FLAG_HTTXCFG_SHORTGI_20M (1 << 5)
#define WC_FLAG_HTTXCFG_SHORTGI_40M (1 << 6)

/*
 * Bit 8 : Enable/Disable 40Mhz Intolarent Bit
 * Bit 17: 20/40Mhz Enable/Disable
 * Bit 23: Short GI in 20 Mhz Enable/Disable
 * Bit 24: Short GI in 40 Mhz Enable/Disable
 * Bit 26: Rx STBC Support Enable/Disable
 * Bit 29: Green Field Enable/Disable
 * 
 * Default Value is set to 0x4800000
 */
#if 0
#define WC_FLAG_HTCAP_INTOL_40M     (1 << 8)
#define WC_FLAG_HTCAP_20_40M        (1 << 17)
#define WC_FLAG_HTCAP_SHORTGI_20M   (1 << 23)
#define WC_FLAG_HTCAP_SHORTGI_40M   (1 << 24)
#define WC_FLAG_HTCAP_RX_STBC       (1 << 26)
#define WC_FLAG_HTCAP_GREEN_FIELD   (1 << 29)
#endif

/*Multicast Mode */
#define WC_PROMISC_MODE     1  /*Receive all packets */
#define WC_MULTICAST_MODE   2  /*Receive multicast packets in multicast list*/
#define WC_ALL_MULTI_MODE   4  /*Receive all multicast packets */

#define WC_MIN_WEP_KEY_SIZE 5
#define WC_MAX_WEP_KEY_SIZE 13


enum rtw_wep_index {
    WC_FLAG_ENCODE_INDEX_1=1,
    WC_FLAG_ENCODE_INDEX_2,
    WC_FLAG_ENCODE_INDEX_3,
    WC_FLAG_ENCODE_INDEX_4
};

typedef enum _wlan_mode {
    WC_FLAG_MODE_ADHOC = 0,
    WC_FLAG_MODE_INFRA,
    WC_FLAG_MODE_MASTER,
    WC_FLAG_MODE_MONITOR,
    WC_FLAG_MODE_AUTO,
    WC_FLAG_MODE_P2P_CLIENT,
    WC_FLAG_MODE_P2P_GO,
    WC_FLAG_MODE_P2P_DEVICE,
} rtw_wlan_mode;

typedef enum _wc_wlan_band {
    WC_FLAG_BAND_ANY = 0,
    WC_FLAG_BAND_11B_ONLY,
    WC_FLAG_BAND_11G_ONLY,
    WC_FLAG_BAND_11BG
} rtw_wc_wlan_band;

typedef enum _wc_wpa_config {
    WC_FLAG_GET_WPA =0,
    WC_FLAG_SET_WPA,
    WC_FLAG_CLEAR_WPA,
} rtw_wc_wpa_config;

typedef enum _wc_wps_mode {
    WC_FLAG_WPS_PIN = 0,
    WC_FLAG_WPS_PBC,
    WC_FLAG_WPS_AUTO
} rtw_wc_wps_mode;

/* 
 * Config Methods
 * 0x0001 : USBA  : 0x0002 : Ethernet
 * 0x0004 : Label : 0x0008 : Display
 * 0x0010 : Ethernet NFC Token
 * 0x0020 : Integrated NFC Token
 * 0x0040 : NFC Interface
 * 0x0080 : Push Button
 * 0x0100 : Keypad
 */
#define WC_WPS_CONFIG_USBA              (1 << 0)
#define WC_WPS_CONFIG_ETHERNET          (1 << 1)
#define WC_WPS_CONFIG_LABEL             (1 << 2)
#define WC_WPS_CONFIG_DISPLAY           (1 << 3)
#define WC_WPS_CONFIG_ETHERNET_NFC      (1 << 4)
#define WC_WPS_CONFIG_INTEG_NFC         (1 << 5)
#define WC_WPS_CONFIG_NFC               (1 << 6)
#define WC_WPS_CONFIG_PBC               (1 << 7)
#define WC_WPS_CONFIG_KEYPAD            (1 << 8)

//XXX:Raj Edit the commands list
/**************************************************************************/
/* WirelessLAN Configuration Commands */
enum rtw_wc_commands {
    RTW_WC_SET_FREQ=0,          /* Set Freq */
    RTW_WC_GET_FREQ,            /* Get Freq */
    RTW_WC_CHANNRL_LIST,        /* Get support channel list*/
    RTW_WC_SET_MODE,            /* Set Mode */
    RTW_WC_GET_MODE,            /* Get Mode */
    RTW_WC_SET_AUTH,            /* Set authentication mode */
    RTW_WC_GET_AUTH,            /* Get authentication mode */
    RTW_WC_SET_BSSID,           /* Set AP MAC addr */
    RTW_WC_GET_BSSID,           /* Get Curr AP MAC addr */
    RTW_WC_SET_ESSID,           /* Set ESSID */
    RTW_WC_GET_ESSID,           /* Get Curr ESSID - 0x10 */
    RTW_WC_SET_RATE,            /* Set Data Rate */
    RTW_WC_GET_RATE,            /* Get Curr Data Rate */    
    RTW_WC_SUPPORT_RATE,        /* Get Support Data Rate */
    RTW_WC_SET_RTS_THRES,       /* Set RTS Thres */
    RTW_WC_GET_RTS_THRES,       /* Get Curr RTS Thres */
    RTW_WC_SET_FRAG_THRES,      /* Set RTS Thres */
    RTW_WC_GET_FRAG_THRES,      /* Get Curr RTS Thres */
    RTW_WC_SET_TXPOW,           /* Set Tx Power */
    RTW_WC_GET_TXPOW,           /* Get Curr Tx Power */
    RTW_WC_SET_PS_MODE,         /* Set PS Mode */
    RTW_WC_GET_PS_MODE,         /* Get PS Mode - 0x20 */
    RTW_WC_SET_SCAN,            /* Start wireless scan */
    RTW_WC_ABORT_SCAN,          /* Abort scan */
    RTW_WC_GET_SCAN,            /* Get wireless scan info */
    RTW_WC_GET_SCAN_IE,         /* Get wireless scan info with ie data*/
    RTW_WC_GET_EXT_SCAN,        /* Get and Store Scan Results */
    RTW_WC_SET_WEP_CONFIG,      /* Set WEP keyinfo */
    RTW_WC_GET_WEP_CONFIG,      /* Get WEP keyinfo */
    RTW_WC_SET_RETRY_LIMIT,     /* Set Retry Limit */
    RTW_WC_GET_RETRY_LIMIT,     /* Get Retry Limit */
	RTW_WC_SET_MAC,				/* Set MAC */
	RTW_WC_GET_MAC,				/* Get MAC */
	RTW_WC_WRITE_ADDR,          /* Set MAC Addr in efuse*/
	RTW_WC_SET_DEAUTH,			/* Set DeAuth */
	RTW_WC_SET_WPS_SESSION,		/* Start / Stop the WPS Session */ 
	RTW_WC_SET_MULTICAST_MODE,	/* Set Multicast Mode */
	RTW_WC_GET_SET_BAND,		/* Set/Get Band */
	RTW_WC_SET_CONN_CB,			/* Set Connection Handler CallBack */
    RTW_WC_SET_GEN_IE,          /* Set Generate IE */
    RTW_WC_GET_GEN_IE,          /* Get Generated IE */
	RTW_WC_SET_PRIVATE,         /* Set Private IOCTL */
    RTW_WC_SET_MLME,            /* Set MLME */
    RTW_WC_ADD_KEY,             /* Add Key*/
    RTW_WC_SET_DEFAULT_KEY,     /* Set Default key*/
    RTW_WC_DEL_KEY,             /* Del Key*/
    RTW_WC_SET_ENCODE_EXT, 	    /* Set Encode Ext */
	RTW_WC_GET_ENCODE_EXT,		/* Get Encode Ext */
	RTW_WC_SET_PMKID,			/* Set PMKID */
    RTW_WC_GET_WIRELESS_STATS,  /* Get Wireless Statistics */
    RTW_WC_SET_WPA_IOCTL,		/* Set WPA Supplicant IOCTL */
	RTW_WC_SET_HOSTAPD_IOCTL,	/* Set Hostapd IOCTL */
	RTW_WC_SET_P2P,				/* Set P2P API's */
	RTW_WC_GET_P2P,				/* Get P2P APi's */
	RTW_WC_GET2_P2P,			/* Get 2 P2P APi's */		
	RTW_WC_SET_PID,				/* Set Product ID */
	RTW_WC_GET_AP_INFO,			/* Get Access Point Info */
	RTW_WC_SET_CHAN_PLAN,		/* Set Channel Plan */
	RTW_WC_SET_WRITE32,			/* Set Write 32 */
	RTW_WC_GET_READ32,			/* Get Read 32 */
	RTW_WC_SET_RF,				/* Set RF */			
	RTW_WC_GET_RF,				/* Get RF */
	RTW_WC_SET_DBG_PORT,		/* Set Debug Port */
	RTW_WC_SET_MP,				/* Set Test Mode - Mass Production Mode */
	RTW_WC_GET_MP,				/* Get Test Mode Statistics - MP Mode */
	RTW_WC_GET_STATUS,			/* Get Media and Driver Status */
	RTW_WC_SET_REGION_CODE,		/* Set Region Code */
	RTW_WC_GET_REGION_CODE,		/* Get Region Code */
	RTW_WC_HAPD_SET_AP,			/* Start AP Mode */
	RTW_WC_HAPD_SET_AP_BEACON,  /* Change AP Beacon */
	RTW_WC_HAPD_SET_AP_CONFIG,	/* Set AP Mode Config*/
	RTW_WC_HAPD_SET_AP_START,   /* Set AP START */
	RTW_WC_HAPD_GET_STA_NUM,    /* GET AP Assoc sta num */
	RTW_WC_HAPD_GET_STA_LIST,   /* GET AP Assoc sta mac addr*/
	RTW_WC_HAPD_GET_NEW_STA_WPA_IE,		/*get wpa ie from sta*/
	RTW_WC_HAPD_STA_FLUSH,			/* flush sta */
	RTW_WC_HAPD_STA_REMOVE,			/*Remove station*/
	RTW_WC_HAPD_WPS_SET_BCN_IE,		/*set beacon ie*/
	RTW_WC_HAPD_WPS_SET_PROBE_RESP_IE,		/*set probe response ie*/
	RTW_WC_HAPD_WPS_SET_ASSOC_RESP_IE,		/*set assoc response ie*/
	RTW_WC_SET_HOSTAPD_SETKEY
}; /* End wc_commands */

/**************************************************************************/
/* RR: newly added - this struct will be used to contain
 * scan info of each BSS , & user get_scan callback will
 * be called with this argument
 */
struct rtk_channel_info
{
    unsigned char				ChannelNum;		/* The channel number. */
	unsigned char	            flags;		/* Scan type such as passive or active scan. */
    unsigned int             ChannelFreq;    /* Channel to freq */
};

struct rtk_APConf {
	unsigned char ssid[32];   /**< SSID array */
    unsigned char ssidLen;                        /**< SSID length */
    unsigned short channel;           /**< Channel */
    unsigned short centerFreq1;       /**< Center frequency 1 */
    unsigned short centerFreq2;       /**< Center frequency 2 */
    unsigned char band;               /**< Band, as enumerated in <b>Ieee80211Band</b> */
    unsigned char width;              /**< Channel width, as enumerated in <b>Ieee80211ChannelWidth</b> */
};

struct rtk_BeaconConf {
    unsigned int interval;      /**< Beacon interval */
    unsigned int DTIMPeriod;    /**< Delivery Traffic Indication Message (DTIM) interval for sending beacons */
    unsigned char *headIEs;       /**< One or more IEs before the TIM IE */
    unsigned int headIEsLength;   /**< Length of the IEs before the TIM IE */
    unsigned char *tailIEs;       /**< One or more IEs after the TIM IE */
    unsigned int tailIEsLength;   /**< Length of the IEs after the TIM IE */
    bool hiddenSSID;        /**< Whether to hide the SSID */
};

struct rtk_WifiStaInfo{
    unsigned char mac[6];
} ;

struct rtk_RateInfo {
    unsigned char    flags;   /**< Flag field, used to indicate a specific rate transmission type of 802.11n */
    unsigned char    mcs;     /**< Modulation and Coding Scheme (MCS) index of the HT/VHT/HE rate */
    unsigned short   legacy;  /**< 100 kbit/s bit rate defined in 802.11a/b/g */
    unsigned char    nss;     /**< Number of streams (for VHT and HE only) */
    unsigned char    resv;    /**< Reserved */
};
struct rtk_StaBssParameters {
    unsigned char  flags;           /**< Flag, used to indicate a specific rate transmission type of 802.11n */
    unsigned char  dtimPeriod;      /**< Delivery Traffic Indication Message (DTIM) period of BSS */
    unsigned short beaconInterval;  /**< Beacon interval */
};

struct rtk_StaFlagUpdate {
    unsigned int mask;  /**< Flag mask */
    unsigned int set;   /**< Flag value */
};

struct rtk_StationInfo {
    unsigned int                          filled;           /**< Flag values of relevant structures */
    unsigned int                          connectedTime;    /**< Duration (in seconds) since the last station connection */
    unsigned int                          inactiveTime;     /**< Duration (in milliseconds) since the last station
                                                         * activity
                                                         */
    unsigned short                          llid;             /**< Local mesh ID */
    unsigned short                          plid;             /**< Peer mesh ID */

    unsigned long long                          rxBytes;          /**< Received bytes */
    unsigned long long                          txBytes;          /**< Transmitted bytes */
    struct rtk_RateInfo                   txRate;           /**< Transmission rate */
    struct rtk_RateInfo                   rxRate;           /**< Receive rate */

    unsigned int                          rxPackets;        /**< Received data packets */
    unsigned int                         txPackets;        /**< Transmitted data packets */
    unsigned int                         txPetries;        /**< Number of retransmissions */
    unsigned int                          txFailed;         /**< Number of failed transmissions */

    unsigned int                          rxDroppedMisc;    /**< Number of receive failures */
    int                          generation;       /**< Generation number */
    struct rtk_StaBssParameters           bssParam;         /**< Current BSS parameters */
    struct rtk_StaFlagUpdate              staFlags;         /**< Station flag masks and values */

    signed long long                           offset;           /**< Time offset of station */
    const unsigned char                    *assocReqIes;      /**< Information Elements (IEs) in Association Request */
    unsigned int                          assocReqIesLen;   /**< IE length in Association Request */
    unsigned int                          beaconLossCount;  /**< Number of beacon loss events triggered */

    unsigned char                           plinkState;       /**< Mesh peer state */
    char                            signal;           /**< Signal strength */
    char                            signalAvg;        /**< Average signal strength */
    unsigned char                           resv1;            /**< Reserved */
};


struct ieee80211_mgmt {
	unsigned short frame_control;
	unsigned short duration;
	unsigned char da[6];
	unsigned char sa[6];
	unsigned char bssid[6];
	unsigned short seq_ctrl;
	union {
		struct {
			unsigned short auth_alg;
			unsigned short auth_transaction;
			unsigned short status_code;
			/* possibly followed by Challenge text */
			unsigned char variable[0];
		} __attribute__((packed)) auth;
		struct {
			unsigned short reason_code;
		} __attribute__((packed))  deauth;
		struct {
			unsigned short capab_info;
			unsigned short listen_interval;
			/* followed by SSID and Supported rates */
			unsigned char variable[0];
		}__attribute__((packed))assoc_req;
		struct {
			unsigned short capab_info;
			unsigned short status_code;
			unsigned short aid;
			/* followed by Supported rates */
			unsigned char variable[0];
		}__attribute__((packed)) assoc_resp, reassoc_resp;
		struct {
			unsigned short capab_info;
			unsigned short listen_interval;
			unsigned char current_ap[6];
			/* followed by SSID and Supported rates */
			unsigned char variable[0];
		}__attribute__((packed)) reassoc_req;
		struct {
			unsigned short reason_code;
		} __attribute__((packed))disassoc;
		struct {
			unsigned long long timestamp;
			unsigned short beacon_int;
			unsigned short capab_info;
			/* followed by some of SSID, Supported rates,
			 * FH Params, DS Params, CF Params, IBSS Params, TIM */
			unsigned char variable[0];
		} __attribute__((packed))beacon;
		struct {
			unsigned long long timestamp;
			unsigned short beacon_int;
			unsigned short capab_info;
			/* followed by some of SSID, Supported rates,
			 * FH Params, DS Params, CF Params, IBSS Params */
			unsigned char variable[0];
		} __attribute__((packed))probe_resp;
	} __attribute__((packed))u;
}__attribute__((packed));

struct rtk_ScannedBssInfo
{
    int                  signal;   /**< Signal strength */
    signed char                  freq;     /**< Center frequency of the channel where the BSS is located */
    unsigned char                  arry[2];  /**< Reserved */
    unsigned int                 mgmtLen;  /**< Management frame length */
    struct ieee80211_mgmt    *mgmt;     /**< Start address of the management frame */
};

struct rtk_ConnetResult {
    uint8_t   bssid[WC_MAC_ADDR_LEN];  /**< MAC address of the AP associated with the station */
    uint16_t  statusCode;           /**< 16-bit status code defined in the IEEE protocol */
    uint8_t  *rspIe;                /**< Association response IE */
    uint8_t  *reqIe;                /**< Association request IE */
    uint32_t  reqIeLen;             /**< Length of the association request IE */
    uint32_t  rspIeLen;             /**< Length of the association response IE */
    uint16_t  connectStatus;        /**< Connection status */
    uint16_t  freq;                 /**< Frequency of the AP */
};


	
typedef struct _wc_bss_info {
    unsigned int mode;              /* 0=ADHOC;1=INFRA(Default) */
    unsigned long channel;
    unsigned int privacy;
    unsigned int phy_mode;          /* 0x00=Auto(Default);0x01=11a; */
    int rate_set[WC_MAX_RATES];     /* (in bps) */
    int strength;                   /* Signal Strength (RSSI) */
    unsigned char bssid[WC_MAC_ADDR_LEN];   /* MAC Addr */
    unsigned char ssid[WC_MAX_ESSID_LEN];   /* SSID */
    unsigned int ssid_len;          /* SSID LEN */
    unsigned int wpa_ie_len;                 /* WPA IE Length */
	unsigned short beacon_period;
    unsigned char wpa_proto;
    unsigned char wpa_pairwise;
    unsigned char wpa_group;
    unsigned char rsn_pairwise;
    unsigned char rsn_group;
    unsigned char wps_ie[256];              /* WPS IE */
	unsigned char wpa_ie[256],rsn_ie[256];  /*WPA IE and RSN IE */
    int wps_ie_len;                      /* WPS IE length */
	int p2p_ie_len;
    unsigned int rsn_ie_len;                 /* RSN IE Length */
    char noise;                    /* Noise Level */
    unsigned char level;    
    unsigned char qual;                     /* LQ */
    unsigned char ap_num;                   /* AP List-Count*/
    int tx_rate;
    unsigned char txpower;                  /* Tx Power */
    unsigned int rts;                       /* RTS Threshold */
    unsigned int frag;                      /* Frag Threshold */
    unsigned char pwr_mgmt;                 /* Power Management */
    int retry_limit;                       /* Retry Limit */   
    unsigned char name[16];
} rtw_wc_bss_info;

typedef struct _wc_bss_ie_info {
    unsigned int mode;              /* 0=ADHOC;1=INFRA(Default) */
    unsigned long channel;
    unsigned int privacy;
    unsigned char strength;         /* Signal Strength (RSSI) */
    unsigned char noise;            /* Noise Level */
    unsigned char bssid[WC_MAC_ADDR_LEN];   /* MAC Addr */
    unsigned char ssid[WC_MAX_ESSID_LEN];   /* SSID */
    unsigned int ssid_len;          /* SSID LEN */
    unsigned short beacon_period;   /* Beacon Period */    
    unsigned int ie_length;         /*ie data length*/
    unsigned char ie_data[1024];    /*ie data pointer*/
} rtw_wc_bss_ie_info;

typedef struct _wc_sta_info {
    unsigned char sta_addr[WC_MAC_ADDR_LEN];    /* Station MAC Addr */
    unsigned char psmode;                    /* power save mode */
    int strength;                               /* Signal Strength */
    int noise;                                  /* Noise Level */
}rtw_wc_sta_info;



typedef struct _wc_event_cb_info {
    unsigned int status; 
    unsigned char mac_addr[WC_MAC_ADDR_LEN];   /* MAC Addr */
    rtw_wc_bss_ie_info *scan_results;
} rtw_wc_event_cb_info;



#define SCAN_AP_LIMIT          (64)

typedef struct _wc_wps_info {
	bool enable;
	int wps_state;	
	unsigned char uuid[40];
	char device_name[33];
	char manufacturer[65];
	char model_name[33];
	char model_number[32];
	char serial_number[33];
	char device_type[14];
	char os_version[10];
	char config_methods[40];
	char pin[9];
} rtw_wc_wps_info;


/** Return type: failure */
#define WC_WPS_SUCCESS      (0)
#define WC_WPS_FAIL         (-1)
#define WC_WPS_OVERLAP      (-2)
#define WC_WPS_TIMEOUT      (-3)
#define WC_WPS_ABORT        (-4)
#define WC_WPS_AUTHFAIL     (-5)

#ifdef __cplusplus
extern "C" {
#endif


/* Copy IPv6 address. */
#define WC_IPV6_ADDRESS(copy_from, copy_to)       \
    do {                                          \
        (copy_to)[0] = (copy_from)[0];            \
        (copy_to)[1] = (copy_from)[1];            \
        (copy_to)[2] = (copy_from)[2];            \
        (copy_to)[3] = (copy_from)[3];            \
    }while(0)


typedef void (*rtw_wc_user_event_cb)(rtw_wc_event_cb_info *event_data);
typedef void (*rtw_wc_conn_cb)(void *devptr, int status);
typedef void (*rtw_wc_wps_cb)(int status);
typedef void (*rtw_wc_scan_cb)(rtw_wc_bss_info *);
typedef void (*rtw_wc_set_scan_cb)(void);
typedef void (*rtw_wc_print_wpa_cb)(char *,int);



#define SIOCGIWAP	0x8B15		/* get access point MAC addresses */
#define IWEVCUSTOM	0x8C02		/* Driver specific ascii string */
#define SIOCGIWSCAN 0x8B19      /* get scanning results */
#define SIOCGIWSCANLIST	  0x8B38		/*  */

#define WPA_ELOOP_EVENT_SCAN_DONE   4
#define WPA_ELOOP_EVENT_SCAN_RESULT 5    /* get scanning results */
#define IWEVASSOCREQIE      0x8C07      /* IEs used in (Re) Assoc Request */
#define IWEVASSOCRESPIE     0x8C08      /* IEs used in (Re) Assoc Response */
#define IWEVREGISTERED      0x8C03
#define IWEVEXPIRED         0x8C04
#define IWEVMICHAELMICFAILURE 0x8C06 
#define IWEVPMKIDCAND       0x8C09

/*
 * Wireless Flags Defininations 
 */

#define WC_EVTXDROP	    0x8C00		/* Packet dropped to excessive retry */
#define WC_EVQUAL	    0x8C01		/* Quality part of statistics (scan) */
#define WC_EVCUSTOM	    0x8C02		/* Driver specific ascii string */
#define WC_EVREGISTERED	0x8C03		/* Discovered a new node (AP mode) */
#define WC_EVEXPIRED	0x8C04		/* Expired a node (AP mode) */
#define WC_EVGENIE	    0x8C05		/* Generic IE (WPA, RSN, WMM, ..) */

/* Max number of char in custom event - use multiple of them if needed */
#define WC_CUSTOM_MAX		256	/* In bytes */

/* Generic information element */
#define WC_GENERIC_IE_MAX	1024

/* MLME requests (SIOCSIWMLME / struct iw_mlme) */
#define WC_MLME_DEAUTH		0
#define WC_MLME_DISASSOC	1
#define WC_MLME_AUTH		2
#define WC_MLME_ASSOC		3

/* SIOCSIWAUTH/SIOCGIWAUTH struct iw_param flags */
#define WC_AUTH_INDEX		0x0FFF
#define WC_AUTH_FLAGS		0xF000

/* SIOCSIWAUTH/SIOCGIWAUTH parameters (0 .. 4095)
 * (IW_AUTH_INDEX mask in struct iw_param flags; this is the index of the
 * parameter that is being set/get to; value will be read/written to
 * struct iw_param value field) */
#define WC_AUTH_WPA_VERSION				0
#define WC_AUTH_CIPHER_PAIRWISE			1
#define WC_AUTH_CIPHER_GROUP			2
#define WC_AUTH_KEY_MGMT				3
#define WC_AUTH_TKIP_COUNTERMEASURES	4
#define WC_AUTH_DROP_UNENCRYPTED		5
#define WC_AUTH_80211_AUTH_ALG			6
#define WC_AUTH_WPA_ENABLED				7
#define WC_AUTH_RX_UNENCRYPTED_EAPOL	8
#define WC_AUTH_ROAMING_CONTROL			9
#define WC_AUTH_PRIVACY_INVOKED			10
#define WC_AUTH_CIPHER_GROUP_MGMT		11
#define WC_AUTH_MFP						12



/* cipher suite selectors */
#define WLAN_CIPHER_SUITE_USE_GROUP	0x000FAC00
#define WLAN_CIPHER_SUITE_WEP40		0x000FAC01
#define WLAN_CIPHER_SUITE_TKIP		0x000FAC02
/* reserved: 				0x000FAC03 */
#define WLAN_CIPHER_SUITE_CCMP		0x000FAC04
#define WLAN_CIPHER_SUITE_WEP104	0x000FAC05
#define WLAN_CIPHER_SUITE_AES_CMAC	0x000FAC06
#define WLAN_CIPHER_SUITE_NO_GROUP_ADDR	0x000FAC07
#define WLAN_CIPHER_SUITE_GCMP		0x000FAC08
#define WLAN_CIPHER_SUITE_GCMP_256	0x000FAC09
#define WLAN_CIPHER_SUITE_CCMP_256	0x000FAC0A
#define WLAN_CIPHER_SUITE_BIP_GMAC_128	0x000FAC0B
#define WLAN_CIPHER_SUITE_BIP_GMAC_256	0x000FAC0C
#define WLAN_CIPHER_SUITE_BIP_CMAC_256	0x000FAC0D

#define WLAN_CIPHER_SUITE_SMS4		0x00147201

#define WLAN_CIPHER_SUITE_CKIP		0x00409600
#define WLAN_CIPHER_SUITE_CKIP_CMIC	0x00409601
#define WLAN_CIPHER_SUITE_CMIC		0x00409602
#define WLAN_CIPHER_SUITE_KRK		0x004096FF /* for nl80211 use only */


/* IW_AUTH_WPA_VERSION values (bit field) */
#define WC_AUTH_WPA_VERSION_DISABLED	0x00000001
#define WC_AUTH_WPA_VERSION_WPA			0x00000002
#define WC_AUTH_WPA_VERSION_WPA2		0x00000004
#define WC_AUTH_AKM_SUITE_PSK           0x00000008
#define WC_AUTH_AKM_SUITE_8021X         0x00000010


/* IW_AUTH_PAIRWISE_CIPHER and IW_AUTH_GROUP_CIPHER values (bit field) */
#define WC_AUTH_CIPHER_NONE		0x00000001
#define WC_AUTH_CIPHER_WEP40	0x00000002
#define WC_AUTH_CIPHER_TKIP		0x00000004
#define WC_AUTH_CIPHER_CCMP		0x00000008
#define WC_AUTH_CIPHER_WEP104	0x00000010
#define WC_AUTH_CIPHER_SMS4     0x00000020

/* IW_AUTH_KEY_MGMT values (bit field) */
#define WC_AUTH_KEY_MGMT_802_1X	1
#define WC_AUTH_KEY_MGMT_PSK	2

/* IW_AUTH_80211_AUTH_ALG values (bit field) */
#define WC_AUTH_ALG_OPEN_SYSTEM		   0x00000001
#define WC_AUTH_ALG_SHARED_KEY		   0x00000002
#define WC_AUTH_ALG_LEAP			   0x00000004
#define WC_AUTH_ALG_WPAPSK			   0x00000008
#define WC_AUTH_ALG_WPA2PSK     	   0x00000010
#define WC_AUTH_ALG_WPA_WPA2_MIXED_PSK 0x00000020
#define WC_AUTH_ALG_WPS				   0x00000040


/* IW_AUTH_ROAMING_CONTROL values */
#define WC_AUTH_ROAMING_ENABLE	0	/* driver/firmware based roaming */
#define WC_AUTH_ROAMING_DISABLE	1	/* user space program used for roaming
					 * control */
/* struct iw_encode_ext ->ext_flags */
#define WC_ENCODE_EXT_TX_SEQ_VALID	0x00000001
#define WC_ENCODE_EXT_RX_SEQ_VALID	0x00000002
#define WC_ENCODE_EXT_GROUP_KEY		0x00000004
#define WC_ENCODE_EXT_SET_TX_KEY	0x00000008

/* SIOCSIWENCODEEXT definitions */
#define WC_ENCODE_SEQ_MAX_SIZE	8
/* struct rtw_wc_encode_ext ->alg */
#define WC_ENCODE_ALG_NONE	0
#define WC_ENCODE_ALG_WEP	1
#define WC_ENCODE_ALG_TKIP	2
#define WC_ENCODE_ALG_CCMP	3
#define WC_ENCODE_ALG_PMK	4
#define WC_ENCODE_ALG_AES_CMAC	5
#define WC_ENCODE_ALG_TKIP_CCMP 6

/* Flags for encoding (along with the token) */
#define WC_ENCODE_INDEX		0x00FF	/* Token index (if needed) */
#define WC_ENCODE_FLAGS		0xFF00	/* Flags defined below */
#define WC_ENCODE_MODE		0xF000	/* Modes defined below */
#define WC_ENCODE_DISABLED	0x8000	/* Encoding disabled */
#define WC_ENCODE_ENABLED	0x0000	/* Encoding enabled */
#define WC_ENCODE_RESTRICTED	0x4000	/* Refuse non-encoded packets */
#define WC_ENCODE_OPEN		0x2000	/* Accept non-encoded packets */
#define WC_ENCODE_NOKEY		0x0800  /* Key is write only, so not present */
#define WC_ENCODE_TEMP		0x0400  /* Temporary key */

struct	rtw_wc_encode_ext
{
	unsigned int ext_flags;
	unsigned char seq[WC_ENCODE_SEQ_MAX_SIZE];
    unsigned char seq_len;
	unsigned char addr[6];
    unsigned int cipher;
    bool pairwise;
    unsigned short alg;
    unsigned char key_index;
	unsigned short key_len;
	unsigned char key[64];
};

#if 0
typedef struct _hostapd_conf {
    unsigned char  bssid[6];
    char ssid[WC_MAX_ESSID_LEN];
    unsigned char  channel_num;
    int wpa_key_mgmt;
    int wpa_pairwise;
    unsigned char key[100];
    unsigned char macaddr_acl;
    int auth_algs;
    unsigned char  wep_idx;
    int wpa;
    /*WPS Related */
	rtw_wc_wps_info wps;
	char supp_rates[35]; //60 90 120 180 240 360 480 540
	char basic_rates[12]; //60 120 240
	char ht_capa[40]; //[SHORT-GI-20][SHORT-GI-40][HT40+]
	char enable11n;
	char enablewmm;
	char h_ssid_set;
} hostapd_conf;
#endif

struct rtw_wlan_request {
	char SSID[WC_MAX_ESSID_LEN];
	char BSSID[20];
	int IsSecurity;
	int SignalLevel;
	int RSSI;
	int Channel;
	int Authentication;
	int Encryption;
	char Key[128]; /* ex) 12345678 */
	int wepKeyIndex;
	int wps;
	int wpsmode; //pin or pbc
	char wpspin[18];  // if mode is pin
	char enable11n;
	char enablewmm;
	char h_ssid_set;
	char ap_scan;
};




#ifdef __cplusplus
}
#endif

enum rtw_set_pmka {
    RTW_WC_CMD_SET_PMKSA,
    RTW_WC_CMD_DEL_PMKSA,
    RTW_WC_CMD_FLUSH_PMKSA,
};

#define WC_FLAG_SUPPORTS_FW_ROAM      0x01
#define WC_FLAG_HAVE_AP_SME           0x02
#define WC_FLAG_AP_PROBE_RESP_OFFLOAD 0x04
#define WC_FLAG_SUPPORTS_SCHED_SCAN   0x08
#define WC_FLAG_P2P_CAPABLE           0x10
#define WC_FLAG_SCAN_THIS_ESSID       0x20


#define WC_PROBE_RESP_OFFLOAD_SUPPORT_WPS      0x01 
#define WC_PROBE_RESP_OFFLOAD_SUPPORT_WPS2     0x02
#define WC_PROBE_RESP_OFFLOAD_SUPPORT_P2P      0x04
#define WC_PROBE_RESP_OFFLOAD_SUPPORT_80211U   0x08

#define IEEE80211_FCTL_FTYPE            0x000c
#define IEEE80211_FCTL_STYPE            0x00f0
#define IEEE80211_FTYPE_MGMT            0x0000
#define IEEE80211_STYPE_ASSOC_REQ       0x0000
#define IEEE80211_STYPE_ASSOC_RESP      0x0010
#define IEEE80211_STYPE_REASSOC_REQ     0x0020
#define IEEE80211_STYPE_REASSOC_RESP    0x0030
#define IEEE80211_STYPE_PROBE_REQ       0x0040
#define IEEE80211_STYPE_PROBE_RESP      0x0050
#define IEEE80211_STYPE_BEACON          0x0080
#define IEEE80211_STYPE_ATIM            0x0090
#define IEEE80211_STYPE_DISASSOC        0x00A0
#define IEEE80211_STYPE_AUTH            0x00B0
#define IEEE80211_STYPE_DEAUTH          0x00C0
#define IEEE80211_STYPE_ACTION          0x00D0

#define IEEE80211_STYPE_DATA            0x0000
#define IEEE80211_STYPE_QOS_DATA		0x0080

#define IEEE80211_FTYPE_MGMT            0x0000
#define IEEE80211_FTYPE_CTL             0x0004
#define IEEE80211_FTYPE_DATA            0x0008


#define IEEE80211_FCTL_VERS		    0x0003
#define IEEE80211_FCTL_FTYPE		0x000c
#define IEEE80211_FCTL_STYPE		0x00f0
#define IEEE80211_FCTL_TODS		    0x0100
#define IEEE80211_FCTL_FROMDS		0x0200
#define IEEE80211_FCTL_MOREFRAGS	0x0400
#define IEEE80211_FCTL_RETRY		0x0800
#define IEEE80211_FCTL_PM		    0x1000
#define IEEE80211_FCTL_MOREDATA		0x2000
#define IEEE80211_FCTL_PROTECTED	0x4000
#define IEEE80211_FCTL_ORDER		0x8000


#define IEEE80211_QOS_CTL_LEN       2
#define IEEE80211_HT_CTL_LEN        4



/* events related */


#define WC_PMKSA_FLUSH   0x01
#define WC_PMKSA_ADD     0x02
#define WC_PMKSA_REMOVE  0x03
#define WC_PMKID_LEN     16

/* IWEVMICHAELMICFAILURE : struct iw_michaelmicfailure ->flags */
#define WC_MICFAILURE_KEY_ID    0x00000003 /* Key ID 0..3 */
#define WC_MICFAILURE_GROUP     0x00000004
#define WC_MICFAILURE_PAIRWISE  0x00000008
#define WC_MICFAILURE_STAKEY    0x00000010
#define WC_MICFAILURE_COUNT     0x00000060 /* 1 or 2 (0 = count not supported)*/



#define WC_PMKID_CAND_PREAUTH  0x00000001


#define WCAPP_CMD_LINE_INTF_ENABLE  (0)

/*
 * Wireless Configuration commands are sent to the driver as driver direct
 * commands. As per NetX Reference Manual the command can be any number after
 * NX_LINK_USER_COMMAND
 * - Change this number if your system uses the same number with WLAN_COMMAND
 */

struct rtw_scan_req {
    int essid_len;
    int flags;
    char essid[32];
};

#define WC_SCAN_ALL_ESSID   0x0001
#define WC_SCAN_THIS_ESSID  0x0002

struct	rtw_wc_pmksa
{
	unsigned int 	cmd; /* IW_PMKSA_* */
	unsigned char	bssid[WC_MAC_ADDR_LEN];
	unsigned char	pmkid[WC_PMKID_LEN];
};

enum {	  
	RTL_WRITE_REG = 1,
	RTL_READ_REG,  //2
	RTL_WRITE_RF,   //3
	RTL_READ_RF,	//4 
	RTL_MP_START,  //5
 	RTL_MP_STOP,   //6
	RTL_MP_RATE,   //7 
	RTL_MP_CHANNEL, // 8
	RTL_MP_BANDWIDTH, // 9
	RTL_MP_TXPOWER, //10
	RTL_MP_ANT_TX,  //11
	RTL_MP_ANT_RX,  //12 
	RTL_MP_CTX,     //13
	RTL_MP_QUERY,    //14
	RTL_MP_ARX,      //15
	RTL_MP_PSD,      //16
  	RTL_MP_PWRTRK,   //17
	RTL_MP_THER,     //18
	RTL_MP_IOCTL,    //19
	RTL_EFUSE_GET,   //20
	RTL_EFUSE_SET,   //21
	RTL_MP_RESET_STATS, //22
	RTL_MP_DUMP,   //23
	RTL_MP_PHYPARA, //24
	RTL_MP_SetRFPathSwh,//25
	RTL_MP_QueryDrvStats, //26
	RTL_MP_SetBT, //27
	RTL_CTA_TEST, //28
	RTL_MP_DISABLE_BT_COEXIST, //29
	RTL_MP_PwrCtlDM, //30
	RTL_MP_NULL, //31
};

#ifndef IEEE_PARAM
#define IEEE_PARAM

#define ETH_ALEN	6
#define	IEEE_CRYPT_ALG_NAME_LEN			16

/* RTL871X_IOCTL_HOSTAPD ioctl() cmd: */
enum {
	RTL871X_HOSTAPD_FLUSH = 1,
	RTL871X_HOSTAPD_ADD_STA = 2,
	RTL871X_HOSTAPD_REMOVE_STA = 3,
	RTL871X_HOSTAPD_GET_INFO_STA = 4,
	/* REMOVED: PRISM2_HOSTAPD_RESET_TXEXC_STA = 5, */
	RTL871X_HOSTAPD_GET_WPAIE_STA = 5,
	RTL871X_SET_ENCRYPTION = 6,
	RTL871X_GET_ENCRYPTION = 7,
	RTL871X_HOSTAPD_SET_FLAGS_STA = 8,
	RTL871X_HOSTAPD_GET_RID = 9,
	RTL871X_HOSTAPD_SET_RID = 10,
	RTL871X_HOSTAPD_SET_ASSOC_AP_ADDR = 11,
	RTL871X_HOSTAPD_SET_GENERIC_ELEMENT = 12,
	RTL871X_HOSTAPD_MLME = 13,
	RTL871X_HOSTAPD_SCAN_REQ = 14,
	RTL871X_HOSTAPD_STA_CLEAR_STATS = 15,
	RTL871X_HOSTAPD_SET_BEACON=16,
	RTL871X_HOSTAPD_SET_WPS_BEACON = 17,
	RTL871X_HOSTAPD_SET_WPS_PROBE_RESP = 18,
	RTL871X_HOSTAPD_SET_WPS_ASSOC_RESP = 19,
	RTL871X_HOSTAPD_SET_HIDDEN_SSID = 20,
	RTL871X_HOSTAPD_SET_MACADDR_ACL = 21,
	RTL871X_HOSTAPD_ACL_ADD_STA = 22,
	RTL871X_HOSTAPD_ACL_REMOVE_STA = 23,
};

struct Key_Params {
    unsigned char *key;    /**< Key content */
    unsigned char *seq;    /**< Content of a Temporal Key Integrity Protocol (TKIP) or Counter Mode Cipher Block Chaining
                      * Message Authentication Code Protocol (CCMP) key
                      */
    int keyLen;  /**< Key length */
    int seqLen;  /**< Length of a TKIP or CCMP key */
    unsigned int cipher; /**< Cipher suite */
};

struct rtw_ieee80211_ht_cap {
	unsigned short 	cap_info;
	unsigned char 	ampdu_params_info;
	unsigned char 	supp_mcs_set[16];
	unsigned short 	extended_ht_cap_info;
	unsigned int		tx_BF_cap_info;
	unsigned char	       antenna_selection_info;
} __attribute__ ((packed));

typedef struct rtw_ieee_param {
	unsigned int cmd;
	unsigned char sta_addr[ETH_ALEN];
       union {
		struct {
			unsigned char name;
			unsigned int value;
		} wpa_param;
		struct {
			unsigned int len;
			unsigned char reserved[32];
			unsigned char data[1];
		} wpa_ie;
	        struct{
			int command;
    			int reason_code;
		} mlme;
		struct {
			unsigned char alg[IEEE_CRYPT_ALG_NAME_LEN];
			unsigned char set_tx;
			unsigned int err;
			unsigned char idx;
			unsigned char seq[8]; /* sequence counter (set: RX, get: TX) */
			unsigned short key_len;
			unsigned char key[1];
		} crypt;
		struct {
			unsigned short aid;
			unsigned short capability;
			int flags;
			unsigned char tx_supp_rates[16];			
			struct rtw_ieee80211_ht_cap ht_cap;
			//struct ieee80211_ht_capabilities ht_cap;
		} add_sta;
		struct {
			unsigned char	reserved[2];//for set max_num_sta
			unsigned char	buf[1];
		} bcn_ie;

	} u;
	   
} rtw_ieee_param;
#endif
int rtw_wc_set_scan(void *devptr,void *scan_req);
int rtw_wc_set_mode(void *devptr, unsigned int mode);
int rtw_wc_set_bssid(void *devptr, char *bssid);
int rtw_wc_set_essid(void *devptr, char *essid, unsigned int ssidlen);
int rtw_wc_set_auth(void *devptr,int flags,unsigned int value);
int rtw_wc_set_pmkid(void *devptr, void *pmkid);
int rtw_wc_get_mac(void *devptr, char *bssid);
int rtw_wc_set_ap(void *devptr, unsigned char *pbuf, unsigned int length);
int rtw_wc_get_ap_new_sta_wpa_ie(void *devptr, unsigned char *sta_addr,unsigned char *piebuf, unsigned int *length);
int rtw_wc_ap_sta_flush(void *devptr);
int rtw_wc_set_remove_sta(void *devptr, unsigned char *sta_addr);
int rtw_wc_ap_set_bcn_ie(void *devptr, unsigned char *ie,unsigned int len);
int rtw_wc_ap_set_probe_resp_ie(void *devptr, unsigned char *ie,unsigned int len);
int rtw_wc_ap_set_assoc_resp_ie(void *devptr, unsigned char *ie,unsigned int len);
int rtw_wc_set_write32(void *devptr, unsigned int bytes,unsigned int data32, unsigned int addr);
int rtw_wc_get_read32(void *devptr, unsigned int bytes, unsigned int addr,unsigned int *data32);
int rtw_wc_set_write_rf(void *devptr, unsigned int path, unsigned int addr,unsigned int data32);
int rtw_wc_get_read_rf(void *devptr, unsigned int path, unsigned int addr,unsigned int *data32);
#endif  /* __WLAN_CONFIG_H__ */
