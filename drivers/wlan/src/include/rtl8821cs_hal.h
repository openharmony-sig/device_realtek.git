/******************************************************************************
 * Copyright 2007 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/
#ifndef _RTL8821CS_HAL_H_
#define _RTL8821CS_HAL_H_

#include <drv_types.h>		/* PADAPTER */

/* rtl8821cs_ops.c */
u8 rtl8821cs_set_hal_ops(PADAPTER);

#endif /* _RTL8821CS_HAL_H_ */
