/******************************************************************************
 * Copyright 2007 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/
#ifndef __RTL8821A_SPEC_H__
#define __RTL8821A_SPEC_H__

#include <drv_conf.h>
/* This file should based on "hal_com_reg.h" */
#include <hal_com_reg.h>
/* Because 8812a and 8821a is the same serial,
 * most of 8821a register definitions are the same as 8812a. */
#include <rtl8812a_spec.h>


/* ************************************************************
 * 8821A Regsiter offset definition
 * ************************************************************ */

/* ************************************************************
 * MAC register
 * ************************************************************ */

/* -----------------------------------------------------
 *	0x0000h ~ 0x00FFh	System Configuration
 * ----------------------------------------------------- */

/* -----------------------------------------------------
 *	0x0100h ~ 0x01FFh	MACTOP General Configuration
 * ----------------------------------------------------- */
#define REG_WOWLAN_WAKE_REASON          REG_MCUTST_WOWLAN

/* -----------------------------------------------------
 *	0x0200h ~ 0x027Fh	TXDMA Configuration
 * ----------------------------------------------------- */

/* -----------------------------------------------------
 *	0x0280h ~ 0x02FFh	RXDMA Configuration
 * ----------------------------------------------------- */

/* -----------------------------------------------------
 *	0x0300h ~ 0x03FFh	PCIe
 * ----------------------------------------------------- */

/* -----------------------------------------------------
 *	0x0400h ~ 0x047Fh	Protocol Configuration
 * ----------------------------------------------------- */

/* -----------------------------------------------------
 *	0x0500h ~ 0x05FFh	EDCA Configuration
 * ----------------------------------------------------- */

/* -----------------------------------------------------
 *	0x0600h ~ 0x07FFh	WMAC Configuration
 * ----------------------------------------------------- */


/* ************************************************************
 * SDIO Bus Specification
 * ************************************************************ */

/* -----------------------------------------------------
 * SDIO CMD Address Mapping
 * ----------------------------------------------------- */

/* -----------------------------------------------------
 * I/O bus domain (Host)
 * ----------------------------------------------------- */

/* -----------------------------------------------------
 * SDIO register
 * ----------------------------------------------------- */
#define SDIO_REG_FREE_TXPG2		0x024
#define SDIO_REG_HCPWM1_8821A	0x025

/* ************************************************************
 * Regsiter Bit and Content definition
 * ************************************************************ */

#endif /* __RTL8821A_SPEC_H__ */
