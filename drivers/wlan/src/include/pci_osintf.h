/******************************************************************************
 * Copyright 2007 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/
#ifndef __PCI_OSINTF_H
#define __PCI_OSINTF_H

#ifdef RTK_129X_PLATFORM
#define PCIE_SLOT1_MEM_START	0x9804F000
#define PCIE_SLOT1_MEM_LEN	0x1000
#define PCIE_SLOT1_CTRL_START	0x9804EC00

#define PCIE_SLOT2_MEM_START	0x9803C000
#define PCIE_SLOT2_MEM_LEN	0x1000
#define PCIE_SLOT2_CTRL_START	0x9803BC00

#define PCIE_MASK_OFFSET	0x100 /* mask offset from CTRL_START */
#define PCIE_TRANSLATE_OFFSET	0x104 /* translate offset from CTRL_START */
#endif

#define PCI_BC_CLK_REQ		BIT0
#define PCI_BC_ASPM_L0s		BIT1
#define PCI_BC_ASPM_L1		BIT2
#define PCI_BC_ASPM_L1Off	BIT3
//#define PCI_BC_ASPM_LTR	BIT4
//#define PCI_BC_ASPM_OBFF	BIT5

void	PlatformClearPciPMEStatus(PADAPTER Adapter);
void	rtw_pci_aspm_config(_adapter *padapter);
void	rtw_pci_aspm_config_l1off_general(_adapter *padapter, u8 eanble);
#ifdef CONFIG_64BIT_DMA
	u8	PlatformEnableDMA64(PADAPTER Adapter);
#endif
#ifdef CONFIG_PCI_DYNAMIC_ASPM
void rtw_pci_set_aspm_lnkctl(_adapter *padapter, u8 mode);
void rtw_pci_set_l1_latency(_adapter *padapter, u8 mode);

static inline void rtw_pci_dynamic_aspm_set_mode(_adapter *padapter, u8 mode)
{
	struct dvobj_priv *pdvobjpriv = adapter_to_dvobj(padapter);
	struct pci_priv	*pcipriv = &(pdvobjpriv->pcipriv);

	if (mode == pcipriv->aspm_mode)
		return;

	pcipriv->aspm_mode = mode;

#ifdef CONFIG_PCI_DYNAMIC_ASPM_LINK_CTRL
	rtw_pci_set_aspm_lnkctl(padapter, mode);
#endif
#ifdef CONFIG_PCI_DYNAMIC_ASPM_L1_LATENCY
	rtw_pci_set_l1_latency(padapter, mode);
#endif
}
#else
#define rtw_pci_dynamic_aspm_set_mode(adapter, mode)
#endif

#endif
