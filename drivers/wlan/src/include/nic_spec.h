/******************************************************************************
 * Copyright 2007 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/


#ifndef __NIC_SPEC_H__
#define __NIC_SPEC_H__

#include <drv_conf.h>

#define RTL8711_MCTRL_		(0x20000)
#define RTL8711_UART_		(0x30000)
#define RTL8711_TIMER_		(0x40000)
#define RTL8711_FINT_		(0x50000)
#define RTL8711_HINT_		(0x50000)
#define RTL8711_GPIO_		(0x60000)
#define RTL8711_WLANCTRL_	(0x200000)
#define RTL8711_WLANFF_		(0xe00000)
#define RTL8711_HCICTRL_	(0x600000)
#define RTL8711_SYSCFG_		(0x620000)
#define RTL8711_SYSCTRL_	(0x620000)
#define RTL8711_MCCTRL_		(0x020000)


#include <rtl8711_regdef.h>

#include <rtl8711_bitdef.h>


#endif /* __RTL8711_SPEC_H__ */
