/******************************************************************************
 * Copyright 2007 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/
#ifndef __RTW_INTF_H__
#define __RTW_INTF_H__

#include "lwip/netifapi.h"

/*****************************************************************************
function name: rtw_wifi_driver_version
Function:  get wifi driver version
Parameter: 	none
return:	0:SUCCESS, -1:FAIL
*****************************************************************************/
void  rtw_wifi_driver_version();

/*****************************************************************************
function name: rtw_wifi_open
Function:  open wifi adapter
Parameter: 	pnetif:return netif
return:	0:SUCCESS, -1:FAIL
*****************************************************************************/
int  rtw_wifi_open(struct netif **pnetif);

/*****************************************************************************
function name: rtw_wifi_close
Function:  close wifi adapter
Parameter: None
return:	0:SUCCESS, -1:FAIL
*****************************************************************************/
int  rtw_wifi_close(void);



/****************************************************************************
function name: rtw_register_promisc_callback
Function: Register Promisc call back
Parameter: callback function
return : no
*****************************************************************************/
void rtw_register_promisc_callback(void (*callback)(unsigned char*, unsigned int));


/****************************************************************************
function name: rtw_register_promisc_callback
Function: Unregister Promisc call back
Parameter: no
return : no
*****************************************************************************/
void rtw_unregister_promisc_callback(void);


/****************************************************************************************************
func name: rtw_promisc_mode_set
Function: promisc mode enable or not
Parameter: enable: 1, disable: 0
return no
****************************************************************************************************/
void rtw_promisc_mode_set(bool benable);


/****************************************************************************************************
func name: rtw_promisc_switch_channel
Function:  switch channel interface
Paramter: channel: 1,2,3,...
return: no
****************************************************************************************************/
void rtw_promisc_switch_channel(unsigned int channel);
#endif

