/******************************************************************************
 * Copyright 2007 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/
#ifndef __RTL8812A_LED_H__
#define __RTL8812A_LED_H__
#ifdef CONFIG_RTW_LED
#ifdef CONFIG_RTW_SW_LED
/* ********************************************************************************
 * Interface to manipulate LED objects.
 * ******************************************************************************** */
#ifdef CONFIG_USB_HCI
void rtl8812au_InitSwLeds(PADAPTER padapter);
void rtl8812au_DeInitSwLeds(PADAPTER padapter);
#endif
#ifdef CONFIG_PCI_HCI
void rtl8812ae_InitSwLeds(PADAPTER padapter);
void rtl8812ae_DeInitSwLeds(PADAPTER padapter);
#endif
#ifdef CONFIG_SDIO_HCI
void rtl8821as_InitSwLeds(PADAPTER padapter);
void rtl8821as_DeInitSwLeds(PADAPTER padapter);
#endif
#endif/*CONFIG_RTW_SW_LED*/
#endif/*#ifdef CONFIG_RTW_LED*/

#ifdef CONFIG_SDIO_HCI
void rtl8821as_init_led_circuit(PADAPTER adapter);
#endif

#endif /*__RTL8812A_LED_H__*/
