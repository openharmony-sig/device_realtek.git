/******************************************************************************
 * Copyright 2007 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/
#ifndef _RTL8822BE_HAL_H_
#define _RTL8822BE_HAL_H_

#include <drv_types.h>		/* PADAPTER */

#define RT_BCN_INT_MASKS	(BIT20 | BIT25 | BIT26 | BIT16)

/* rtl8822be_ops.c */
void UpdateInterruptMask8822BE(PADAPTER, u32 AddMSR, u32 AddMSR1, u32 RemoveMSR, u32 RemoveMSR1);
u16 get_txbd_rw_reg(u16 q_idx);


#endif /* _RTL8822BE_HAL_H_ */
