/******************************************************************************
 * Copyright 2007 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/
#ifndef _RTL8822BS_HAL_H_
#define _RTL8822BS_HAL_H_

#include <drv_types.h>		/* PADAPTER */

/* rtl8822bs_ops.c */
void rtl8822bs_set_hal_ops(PADAPTER);

#if defined(CONFIG_WOWLAN) || defined(CONFIG_AP_WOWLAN)
void rtl8822bs_disable_interrupt_but_cpwm2(PADAPTER adapter);
#endif

/* rtl8822bs_xmit.c */
s32 rtl8822bs_dequeue_writeport(PADAPTER);
#define _dequeue_writeport(a)	rtl8822bs_dequeue_writeport(a)

#endif /* _RTL8822BS_HAL_H_ */
