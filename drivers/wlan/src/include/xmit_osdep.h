/******************************************************************************
 * Copyright 2007 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/
#ifndef __XMIT_OSDEP_H_
#define __XMIT_OSDEP_H_


struct pkt_file {
	_pkt *pkt;
	SIZE_T pkt_len;	 /* the remainder length of the open_file */
	_buffer *cur_buffer;
	u8 *buf_start;
	u8 *cur_addr;
	SIZE_T buf_len;
};

#ifdef PLATFORM_WINDOWS

#ifdef PLATFORM_OS_XP
#ifdef CONFIG_USB_HCI
#include <usb.h>
#include <usbdlib.h>
#include <usbioctl.h>
#endif
#endif

#ifdef CONFIG_GSPI_HCI
	#define NR_XMITFRAME     64
#else
	#define NR_XMITFRAME     128
#endif

#define ETH_ALEN	6

extern NDIS_STATUS rtw_xmit_entry(
	_nic_hdl		cnxt,
	NDIS_PACKET		*pkt,
	u32				flags
);

#endif /* PLATFORM_WINDOWS */

#ifdef PLATFORM_FREEBSD
#define NR_XMITFRAME	256
extern int rtw_xmit_entry(_pkt *pkt, _nic_hdl pnetdev);
extern void rtw_xmit_entry_wrap(struct ifnet *pifp);
#endif /* PLATFORM_FREEBSD */

#if defined(PLATFORM_LINUX) || defined(PLATFORM_LITEOS)||defined(PLATFORM_HARMONY)
#define NR_XMITFRAME	256

struct xmit_priv;
struct pkt_attrib;
struct sta_xmit_priv;
struct xmit_frame;
struct xmit_buf;
#endif

#if defined(PLATFORM_LINUX) || defined(PLATFORM_LITEOS)
extern int _rtw_xmit_entry(_pkt *pkt, _nic_hdl pnetdev);
extern int rtw_xmit_entry(_pkt *pkt, _nic_hdl pnetdev);
#elif defined(PLATFORM_HARMONY)
extern int _rtw_xmit_entry(_nic_pkt *pkt, _nic_hdl pnetdev);
extern int rtw_xmit_entry( _nic_hdl pnetdev, _nic_pkt *pkt);
#endif

void rtw_os_xmit_schedule(_adapter *padapter);

int rtw_os_xmit_resource_alloc(_adapter *padapter, struct xmit_buf *pxmitbuf, u32 alloc_sz, u8 flag);
void rtw_os_xmit_resource_free(_adapter *padapter, struct xmit_buf *pxmitbuf, u32 free_sz, u8 flag);

extern void rtw_set_tx_chksum_offload(_pkt *pkt, struct pkt_attrib *pattrib);

extern uint rtw_remainder_len(struct pkt_file *pfile);
extern void _rtw_open_pktfile(_pkt *pkt, struct pkt_file *pfile);
extern uint _rtw_pktfile_read(struct pkt_file *pfile, u8 *rmem, uint rlen);
extern sint rtw_endofpktfile(struct pkt_file *pfile);

extern void rtw_os_pkt_complete(_adapter *padapter, _pkt *pkt);
extern void rtw_os_xmit_complete(_adapter *padapter, struct xmit_frame *pxframe);

void rtw_os_wake_queue_at_free_stainfo(_adapter *padapter, int *qcnt_freed);

void dump_os_queue(void *sel, _adapter *padapter);

#endif /* __XMIT_OSDEP_H_ */
