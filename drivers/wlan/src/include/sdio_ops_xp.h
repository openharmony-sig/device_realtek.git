/******************************************************************************
 * Copyright 2007 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/
#ifndef _SDIO_OPS_XP_H_
#define _SDIO_OPS_XP_H_

#include <drv_conf.h>
#include <osdep_service.h>
#include <drv_types.h>
#include <osdep_intf.h>


#ifdef PLATFORM_OS_XP


extern u8 sdbus_cmd52r_xp(struct intf_priv *pintfpriv, u32 addr);


extern void sdbus_cmd52w_xp(struct intf_priv *pintfpriv, u32 addr, u8 val8);


uint sdbus_read_blocks_to_membuf_xp(struct intf_priv *pintfpriv, u32 addr, u32 cnt, u8 *pbuf);

extern uint sdbus_read_bytes_to_membuf_xp(struct intf_priv *pintfpriv, u32 addr, u32 cnt, u8 *pbuf);


extern uint sdbus_write_blocks_from_membuf_xp(struct intf_priv *pintfpriv, u32 addr, u32 cnt, u8 *pbuf, u8 async);

extern uint sdbus_write_bytes_from_membuf_xp(struct intf_priv *pintfpriv, u32 addr, u32 cnt, u8 *pbuf);
extern u8 sdbus_func1cmd52r_xp(struct intf_priv *pintfpriv, u32 addr);
extern void sdbus_func1cmd52w_xp(struct intf_priv *pintfpriv, u32 addr, u8 val8);
extern uint sdbus_read_reg(struct intf_priv *pintfpriv, u32 addr, u32 cnt, void *pdata);
extern uint sdbus_write_reg(struct intf_priv *pintfpriv, u32 addr, u32 cnt, void *pdata);
extern void sdio_read_int(_adapter *padapter, u32 addr, u8 sz, void *pdata);

#endif

#endif
