/******************************************************************************
 * Copyright 2007 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/
#ifndef __DRV_TYPES_GSPI_H__
#define __DRV_TYPES_GSPI_H__

/* SPI Header Files */
#ifdef PLATFORM_LINUX
	#include <linux/platform_device.h>
	#include <linux/spi/spi.h>
	#include <linux/gpio.h>
	/* #include <mach/ldo.h> */
	#include <asm/mach-types.h>
	#include <asm/gpio.h>
	#include <asm/io.h>
	#include <mach/board.h>
	#include <mach/hardware.h>
	#include <mach/irqs.h>
	#include <custom_gpio.h>
#endif


typedef struct gspi_data {
	u8  func_number;

	u8  tx_block_mode;
	u8  rx_block_mode;
	u32 block_transfer_len;

#ifdef PLATFORM_LINUX
	struct spi_device *func;

	struct workqueue_struct *priv_wq;
	struct delayed_work irq_work;
#endif
} GSPI_DATA, *PGSPI_DATA;

#endif /*  #ifndef __DRV_TYPES_GSPI_H__ */
