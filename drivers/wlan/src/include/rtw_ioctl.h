/******************************************************************************
 * Copyright 2007 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/
#ifndef _RTW_IOCTL_H_
#define _RTW_IOCTL_H_

enum oid_type {
	QUERY_OID,
	SET_OID
};

struct oid_par_priv {
	void		*adapter_context;
	NDIS_OID	oid;
	void		*information_buf;
	u32		information_buf_len;
	u32		*bytes_rw;
	u32		*bytes_needed;
	enum oid_type	type_of_oid;
	u32		dbg;
};

#if defined(PLATFORM_LINUX) && defined(CONFIG_WIRELESS_EXT)
extern struct iw_handler_def  rtw_handlers_def;
#endif

extern void rtw_request_wps_pbc_event(_adapter *padapter);

#ifdef CONFIG_APPEND_VENDOR_IE_ENABLE
extern int rtw_vendor_ie_get_raw_data(struct net_device *, u32, char *, u32);
extern int rtw_vendor_ie_get_data(struct net_device*, int , char*);
extern int rtw_vendor_ie_get(struct net_device *, struct iw_request_info *, union iwreq_data *, char *);
extern int rtw_vendor_ie_set(struct net_device*, struct iw_request_info*, union iwreq_data*, char*);
#endif

#endif /*  #ifndef __INC_CEINFO_ */
