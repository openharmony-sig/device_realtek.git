/******************************************************************************
 * Copyright 2007 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/
#ifndef __PCI_HAL_H__
#define __PCI_HAL_H__

#ifdef CONFIG_RTL8188E
	void rtl8188ee_set_hal_ops(_adapter *padapter);
#endif

#if defined(CONFIG_RTL8812A) || defined(CONFIG_RTL8821A)
	void rtl8812ae_set_hal_ops(_adapter *padapter);
#endif

#if defined(CONFIG_RTL8192E)
	void rtl8192ee_set_hal_ops(_adapter *padapter);
#endif

#if defined(CONFIG_RTL8192F)
	void rtl8192fe_set_hal_ops(_adapter *padapter);
#endif

#ifdef CONFIG_RTL8723B
	void rtl8723be_set_hal_ops(_adapter *padapter);
#endif

#ifdef CONFIG_RTL8723D
	void rtl8723de_set_hal_ops(_adapter *padapter);
#endif

#ifdef CONFIG_RTL8814A
	void rtl8814ae_set_hal_ops(_adapter *padapter);
#endif

#ifdef CONFIG_RTL8822B
	void rtl8822be_set_hal_ops(PADAPTER padapter);
#endif

#ifdef CONFIG_RTL8822C
	void rtl8822ce_set_hal_ops(PADAPTER padapter);
#endif

#ifdef CONFIG_RTL8814B
	void rtl8814be_set_hal_ops(PADAPTER padapter);
#endif

u8 rtw_set_hal_ops(_adapter *padapter);

#endif /* __PCIE_HAL_H__ */
