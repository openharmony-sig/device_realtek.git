/******************************************************************************
 * Copyright 2007 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/
#ifndef __RTW_RSON_H_
#define __RTW_RSON_H_


#define RTW_RSON_VER						1

#define RTW_RSON_SCORE_NOTSUP			0x0
#define RTW_RSON_SCORE_NOTCNNT			0x1
#define RTW_RSON_SCORE_MAX				0xFF
#define RTW_RSON_HC_NOTREADY			0xFF
#define RTW_RSON_HC_ROOT				0x0
#define RTW_RSON_ALLOWCONNECT			0x1
#define RTW_RSON_DENYCONNECT			0x0



/*	for rtw self-origanization spec 1	*/
struct rtw_rson_struct {
	u8 ver;
	u32 id;
	u8 hopcnt;
	u8 connectible;
	u8 loading;
	u8 res[16];
} __attribute__((__packed__));

void init_rtw_rson_data(struct dvobj_priv *dvobj);
void rtw_rson_get_property_str(_adapter *padapter, char *rson_data_str);
int rtw_rson_set_property(_adapter *padapter, char *field, char *value);
int rtw_rson_choose(struct wlan_network **candidate, struct wlan_network *competitor);
int rtw_get_rson_struct(WLAN_BSSID_EX *bssid, struct  rtw_rson_struct *rson_data);
u8 rtw_cal_rson_score(struct rtw_rson_struct *cand_rson_data, NDIS_802_11_RSSI  Rssi);
void rtw_rson_handle_ie(WLAN_BSSID_EX *bssid, u8 ie_offset);
u32 rtw_rson_append_ie(_adapter *padapter, unsigned char *pframe, u32 *len);
void rtw_rson_do_disconnect(_adapter *padapter);
void rtw_rson_join_done(_adapter *padapter);
int rtw_rson_isupdate_roamcan(struct mlme_priv *mlme, struct wlan_network **candidate, struct wlan_network *competitor);
void rtw_rson_show_survey_info(struct seq_file *m, _list *plist, _list *phead);
u8 rtw_rson_ap_check_sta(_adapter *padapter, u8 *pframe, uint pkt_len, unsigned short ie_offset);
u8 rtw_rson_scan_wk_cmd(_adapter *padapter, int op);
void rtw_rson_scan_cmd_hdl(_adapter *padapter, int op);
#endif /* __RTW_RSON_H_ */
