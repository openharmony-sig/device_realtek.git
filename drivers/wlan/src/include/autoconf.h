/******************************************************************************
 * Copyright 2007 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/
 
/*
 * Public General Config
 */
#define AUTOCONF_INCLUDED

#define RTL871X_MODULE_NAME "8821CS"
#define DRV_NAME "rtl8821cs"


#ifndef CONFIG_RTL8821C
#error "It should be defined in makefile"
#define CONFIG_RTL8821C
#endif

#define CONFIG_SDIO_HCI
#define PLATFORM_HARMONY
//need fix platform if 
#define SDIO_BUS_NUM 1   //for  hi3516DV300
/***************************linux macro*********************************/
/*
 * Wi-Fi Functions Config
 */
#define CONFIG_RECV_REORDERING_CTRL	1

#define CONFIG_80211N_HT	1
#define CONFIG_80211AC_VHT
#ifdef CONFIG_80211AC_VHT
	#ifndef CONFIG_80211N_HT
		#define CONFIG_80211N_HT
	#endif
#endif
#ifdef CONFIG_80211N_HT
	/* #define CONFIG_BEAMFORMING */
#endif

#define CONFIG_IEEE80211_BAND_5GHZ

/* set CONFIG_IOCTL_CFG80211 from Makefile */
#ifdef CONFIG_IOCTL_CFG80211
	/*
	 * Indecate new sta asoc through cfg80211_new_sta
	 * If kernel version >= 3.2 or
	 * version < 3.2 but already apply cfg80211 patch,
	 * RTW_USE_CFG80211_STA_EVENT must be defiend!
	 */
	/* Set RTW_USE_CFG80211_STA_EVENT from Makefile */
	/* #define RTW_USE_CFG80211_STA_EVENT */ /* Indecate new sta asoc through cfg80211_new_sta */
	#define CONFIG_CFG80211_FORCE_COMPATIBLE_2_6_37_UNDER
	/* #define CONFIG_DEBUG_CFG80211 */
	/* #define CONFIG_DRV_ISSUE_PROV_REQ */ /* IOT FOR S2 */
	#define CONFIG_SET_SCAN_DENY_TIMER
#endif

#define CONFIG_AP_MODE	1
#ifdef CONFIG_AP_MODE
	/* #define CONFIG_INTERRUPT_BASED_TXBCN */ /* Tx Beacon when driver BCN_OK ,BCN_ERR interrupt occurs */
	#if defined(CONFIG_CONCURRENT_MODE) && defined(CONFIG_INTERRUPT_BASED_TXBCN)
		#undef CONFIG_INTERRUPT_BASED_TXBCN
	#endif
	#ifdef CONFIG_INTERRUPT_BASED_TXBCN
		/* #define CONFIG_INTERRUPT_BASED_TXBCN_EARLY_INT */
		#define CONFIG_INTERRUPT_BASED_TXBCN_BCN_OK_ERR
	#endif

	#define CONFIG_NATIVEAP_MLME	1
	#ifndef CONFIG_NATIVEAP_MLME
		#define CONFIG_HOSTAPD_MLME
	#endif
	#define CONFIG_FIND_BEST_CHANNEL	1
#endif

//#define CONFIG_P2P 1
#ifdef CONFIG_P2P
	/* The CONFIG_WFD is for supporting the Wi-Fi display */
	#define CONFIG_WFD

	#define CONFIG_P2P_REMOVE_GROUP_INFO

	/* #define CONFIG_DBG_P2P */

	#define CONFIG_P2P_PS
	/* #define CONFIG_P2P_IPS */
	#define CONFIG_P2P_OP_CHK_SOCIAL_CH
	#define CONFIG_CFG80211_ONECHANNEL_UNDER_CONCURRENT  /* replace CONFIG_P2P_CHK_INVITE_CH_LIST flag */
	/*#define CONFIG_P2P_INVITE_IOT*/
#endif

/* Set CONFIG_TDLS from Makefile */
#ifdef CONFIG_TDLS
	#define CONFIG_TDLS_DRIVER_SETUP
/*
	#ifndef CONFIG_WFD
		#define CONFIG_WFD
	#endif
*/
	/* #define CONFIG_TDLS_AUTOSETUP */
	#define CONFIG_TDLS_AUTOCHECKALIVE
	#define CONFIG_TDLS_CH_SW		/* Enable "CONFIG_TDLS_CH_SW" by default, however limit it to only work in wifi logo test mode but not in normal mode currently */
	
	#define CONFIG_TDLS_CH_SW_OPER_OFFLOAD
#endif

#define RTW_HALMAC		/* Use HALMAC architecture, necessary for 8821C */
#define CONFIG_EMBEDDED_FWIMG	1


#define CONFIG_XMIT_ACK
#ifdef CONFIG_XMIT_ACK
	#define CONFIG_ACTIVE_KEEP_ALIVE_CHECK
#endif

#define CONFIG_C2H_PACKET_EN

#define CONFIG_BT_SUPPORT 0

#define CONFIG_DFS	1

#ifdef CONFIG_POWER_SAVING
#define CONFIG_IPS
#ifdef CONFIG_IPS
/* #define CONFIG_IPS_LEVEL_2*/ /*enable this to set default IPS mode to IPS_LEVEL_2*/
#define CONFIG_IPS_CHECK_IN_WD /* Do IPS Check in WatchDog.	*/
#endif
/* #define SUPPORT_HW_RFOFF_DETECTED*/

#define CONFIG_LPS
#if defined(CONFIG_LPS)
/* #define CONFIG_LPS_LCLK*/
#endif

#ifdef CONFIG_LPS_LCLK
	#ifdef CONFIG_POWER_SAVING
		/* #define CONFIG_XMIT_THREAD_MODE */
	#endif
	#ifndef CONFIG_SUPPORT_USB_INT
		#define LPS_RPWM_WAIT_MS 300
		#define CONFIG_DETECT_CPWM_BY_POLLING
	#endif /* !CONFIG_SUPPORT_USB_INT */
	/* #define DBG_CHECK_FW_PS_STATE */
#endif

	#ifdef CONFIG_LPS
		#define CONFIG_WMMPS_STA 1
	#endif /* CONFIG_LPS */
#endif /*CONFIG_POWER_SAVING*/

#define CONFIG_AP_MODE	1
#ifdef CONFIG_AP_MODE
	/* #define CONFIG_INTERRUPT_BASED_TXBCN */ /* Tx Beacon when driver BCN_OK ,BCN_ERR interrupt occurs */
	#if defined(CONFIG_CONCURRENT_MODE) && defined(CONFIG_INTERRUPT_BASED_TXBCN)
		#undef CONFIG_INTERRUPT_BASED_TXBCN
	#endif
	#ifdef CONFIG_INTERRUPT_BASED_TXBCN
		/* #define CONFIG_INTERRUPT_BASED_TXBCN_EARLY_INT */
		#define CONFIG_INTERRUPT_BASED_TXBCN_BCN_OK_ERR
	#endif

	#define CONFIG_NATIVEAP_MLME	1
	#ifndef CONFIG_NATIVEAP_MLME
		#define CONFIG_HOSTAPD_MLME
	#endif
	#define CONFIG_FIND_BEST_CHANNEL	1
#endif



#define CONFIG_SKB_COPY	1/* amsdu */

//#define CONFIG_RTW_LED
#ifdef CONFIG_RTW_LED
	#define CONFIG_RTW_SW_LED
	#ifdef CONFIG_RTW_SW_LED
		/* #define CONFIG_RTW_LED_HANDLED_BY_CMD_THREAD */
	#endif
#endif /* CONFIG_RTW_LED */

//#define CONFIG_GLOBAL_UI_PID

//#define CONFIG_LAYER2_ROAMING
//#define CONFIG_LAYER2_ROAMING_RESUME
/*#define CONFIG_ADAPTOR_INFO_CACHING_FILE */ /* now just applied on 8192cu only, should make it general... */
/*#define CONFIG_RESUME_IN_WORKQUEUE */
/*#define CONFIG_SET_SCAN_DENY_TIMER */
#define CONFIG_LONG_DELAY_ISSUE
#define CONFIG_NEW_SIGNAL_STAT_PROCESS


/* #define CONFIG_SIGNAL_DISPLAY_DBM */ /*display RX signal with dbm */
#ifdef CONFIG_SIGNAL_DISPLAY_DBM
/* #define CONFIG_BACKGROUND_NOISE_MONITOR */
#endif
#define RTW_NOTCH_FILTER 0 /* 0:Disable, 1:Enable, */

/*
 * Interface  Related Config
 */

//#ifndef CONFIG_MINIMAL_MEMORY_USAGE
//	#define CONFIG_USB_TX_AGGREGATION	1
//	#define CONFIG_USB_RX_AGGREGATION	1
//#endif

/* #define CONFIG_USE_USB_BUFFER_ALLOC_TX*/	/* Trade-off: For TX path, improve stability on some platforms, but may cause performance degrade on other platforms. */
/* #define CONFIG_USE_USB_BUFFER_ALLOC_RX*/ /* For RX path */
#ifdef CONFIG_USE_USB_BUFFER_ALLOC_RX

#else
	#define CONFIG_PREALLOC_RECV_SKB
	#ifdef CONFIG_PREALLOC_RECV_SKB
		/* #define CONFIG_FIX_NR_BULKIN_BUFFER */ /* only use PREALLOC_RECV_SKB buffer, don't alloc skb at runtime */
	#endif
#endif


#ifdef CONFIG_WOWLAN
	#define CONFIG_GTK_OL
	/* #define CONFIG_ARP_KEEP_ALIVE */
#endif /* CONFIG_WOWLAN */

#ifdef CONFIG_GPIO_WAKEUP
	#ifndef WAKEUP_GPIO_IDX
		#define WAKEUP_GPIO_IDX	6	/* WIFI Chip Side */
	#endif /* WAKEUP_GPIO_IDX */
#endif /* CONFIG_GPIO_WAKEUP */

#define DISABLE_BB_RF	0

#ifdef CONFIG_MP_INCLUDED
	#define MP_DRIVER 1
	#define CONFIG_MP_IWPRIV_SUPPORT	1
	/*
	 #undef CONFIG_USB_TX_AGGREGATION
	 #undef CONFIG_USB_RX_AGGREGATION
	*/
#else
	#define MP_DRIVER 0
#endif

#if defined(CONFIG_PLATFORM_ACTIONS_ATM702X)
	#ifdef CONFIG_USB_TX_AGGREGATION
		#undef CONFIG_USB_TX_AGGREGATION
	#endif
	#ifndef CONFIG_USE_USB_BUFFER_ALLOC_TX
		#define CONFIG_USE_USB_BUFFER_ALLOC_TX
	#endif
	#ifndef CONFIG_USE_USB_BUFFER_ALLOC_RX
		#define CONFIG_USE_USB_BUFFER_ALLOC_RX
	#endif
#endif


#ifdef CONFIG_BT_COEXIST
	/* for ODM and outsrc BT-Coex */
	#define BT_30_SUPPORT 1
	#ifndef CONFIG_LPS
		/* download reserved page to FW */
		#define CONFIG_LPS
	#endif
#else /* !CONFIG_BT_COEXIST */
	#define BT_30_SUPPORT 0
#endif /* CONFIG_BT_COEXIST */

/*********************origion macro**********************/
//#define CONFIG_AUTO_CHNL_SEL_NHM
//#define DBG_AUTO_CHNL_SEL_NHM

//#define CONFIG_DLFW_TXPKT

//#define CONFIG_IOCTL_CFG80211 1
#define USE_RTW_SKB_DEFINE //for liteos

/*
 * Internal  General Config
 */
 
#define CONFIG_SDIO_CHK_HCI_RESUME
#define CONFIG_TX_AGGREGATION
#define CONFIG_SDIO_RX_COPY
#define CONFIG_XMIT_THREAD_MODE
//#define CONFIG_SDIO_TX_ENABLE_AVAL_INT
 
#define CONFIG_RECV_THREAD_MODE
//#define CONFIG_H2CLBK

//#define CONFIG_FILE_FWIMG


//#define CONFIG_RF_GAIN_OFFSET




#define CONFIG_DO_TIMER_CALLBACK_HANDLER_IN_THREAD 1 //xhl add for run callback fun in thread
//#define CONFIG_EFUSE_FROM_ARRAY

//#define CONFIG_TCP_CSUM_OFFLOAD_RX	1

//#define CONFIG_DRVEXT_MODULE	1

//#define USB_INTERFERENCE_ISSUE // this should be checked in all usb interface

//#define CONFIG_TX_MCAST2UNI		/*Support IP multicast->unicast*/
//#define CONFIG_CHECK_AC_LIFETIME 1	// Check packet lifetime of 4 ACs.

//#define CONFIG_REDUCE_USB_TX_INT	1	// Trade-off: Improve performance, but may cause TX URBs blocked by USB Host/Bus driver on few platforms.
//#define CONFIG_EASY_REPLACEMENT	1

/* 
 * USB VENDOR REQ BUFFER ALLOCATION METHOD
 * if not set we'll use function local variable (stack memory)
 */
//#define CONFIG_USB_VENDOR_REQ_BUFFER_DYNAMIC_ALLOCATE

//#define CONFIG_USB_SUPPORT_ASYNC_VDN_REQ 1
/*
 * HAL	Related Config
 */

#define CONFIG_RX_PACKET_APPEND_FCS

//#define CONFIG_ONLY_ONE_OUT_EP_TO_LOW 0

#define CONFIG_OUT_EP_WIFI_MODE 0


#define CONFIG_ADHOC_WORKAROUND_SETTING 1

#define ENABLE_NEW_RFE_TYPE 0

#define CONFIG_80211D

#define CONFIG_ATTEMPT_TO_FIX_AP_BEACON_ERROR

/*
 * Debug Related Config
 */
//#define NO_PRINT_LOG //for liteos
#define DBG	0

#define CONFIG_DEBUG 
#define CONFIG_RTW_DEBUG 

//#define CONFIG_PROC_DEBUG

//#define DBG_CONFIG_ERROR_DETECT
//#define DBG_CONFIG_ERROR_DETECT_INT
//#define DBG_CONFIG_ERROR_RESET

//#define DBG_IO
//#define DBG_DELAY_OS
//#define DBG_MEM_ALLOC
//#define DBG_IOCTL

//#define DBG_TX
//#define DBG_XMIT_BUF
//#define DBG_XMIT_BUF_EXT
//#define DBG_TX_DROP_FRAME

//#define DBG_RX_DROP_FRAME
//#define DBG_RX_SEQ
//#define DBG_RX_SIGNAL_DISPLAY_PROCESSING
//#define DBG_RX_SIGNAL_DISPLAY_SSID_MONITORED "jeff-ap"



//#define DBG_SHOW_MCUFWDL_BEFORE_51_ENABLE
//#define DBG_ROAMING_TEST

//#define DBG_HAL_INIT_PROFILING

//#define DBG_MEMORY_LEAK	1
/*#define DBG_UDP_PKT_LOSE_11AC */


