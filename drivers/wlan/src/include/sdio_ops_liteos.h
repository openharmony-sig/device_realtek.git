/******************************************************************************
 * Copyright 2007 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/
#ifndef __SDIO_OPS_LITEOS_H__
#define __SDIO_OPS_LITEOS_H__

#define SDIO_ERR_VAL8	0xEA
#define SDIO_ERR_VAL16	0xEAEA
#define SDIO_ERR_VAL32	0xEAEAEAEA

#if 0
u8 sd_f0_read8(struct intf_hdl *pintfhdl, u32 addr, s32 *err);
void sd_f0_write8(struct intf_hdl *pintfhdl, u32 addr, u8 v, s32 *err);

s32 _sd_cmd52_read(struct intf_hdl *pintfhdl, u32 addr, u32 cnt, u8 *pdata);
s32 _sd_cmd52_write(struct intf_hdl *pintfhdl, u32 addr, u32 cnt, u8 *pdata);
s32 sd_cmd52_read(struct intf_hdl *pintfhdl, u32 addr, u32 cnt, u8 *pdata);
s32 sd_cmd52_write(struct intf_hdl *pintfhdl, u32 addr, u32 cnt, u8 *pdata);

u8 _sd_read8(struct intf_hdl *pintfhdl, u32 addr, s32 *err);
u8 sd_read8(struct intf_hdl *pintfhdl, u32 addr, s32 *err);
u16 sd_read16(struct intf_hdl *pintfhdl, u32 addr, s32 *err);
u32 _sd_read32(struct intf_hdl *pintfhdl, u32 addr, s32 *err);
u32 sd_read32(struct intf_hdl *pintfhdl, u32 addr, s32 *err);
s32 _sd_read(struct intf_hdl *pintfhdl, u32 addr, u32 cnt, void *pdata);
s32 sd_read(struct intf_hdl *pintfhdl, u32 addr, u32 cnt, void *pdata);
void sd_write8(struct intf_hdl *pintfhdl, u32 addr, u8 v, s32 *err);
void sd_write16(struct intf_hdl *pintfhdl, u32 addr, u16 v, s32 *err);
void _sd_write32(struct intf_hdl *pintfhdl, u32 addr, u32 v, s32 *err);
void sd_write32(struct intf_hdl *pintfhdl, u32 addr, u32 v, s32 *err);
s32 _sd_write(struct intf_hdl *pintfhdl, u32 addr, u32 cnt, void *pdata);
s32 sd_write(struct intf_hdl *pintfhdl, u32 addr, u32 cnt, void *pdata);


void rtw_sdio_set_irq_thd(struct dvobj_priv *dvobj, _thread_hdl_ thd_hdl);
#endif
#endif

