/******************************************************************************
 * Copyright 2007 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/
#ifndef __SDIO_HAL_H__
#define __SDIO_HAL_H__

void sd_int_dpc(PADAPTER padapter);
u8 rtw_set_hal_ops(_adapter *padapter);

#ifdef CONFIG_RTL8188E
void rtl8188es_set_hal_ops(PADAPTER padapter);
#endif

#ifdef CONFIG_RTL8723B
void rtl8723bs_set_hal_ops(PADAPTER padapter);
#endif

#ifdef CONFIG_RTL8821A
void rtl8821as_set_hal_ops(PADAPTER padapter);
#endif

#ifdef CONFIG_RTL8192E
void rtl8192es_set_hal_ops(PADAPTER padapter);
#endif

#ifdef CONFIG_RTL8703B
void rtl8703bs_set_hal_ops(PADAPTER padapter);
#endif

#ifdef CONFIG_RTL8723D
void rtl8723ds_set_hal_ops(PADAPTER padapter);
#endif

#ifdef CONFIG_RTL8188F
void rtl8188fs_set_hal_ops(PADAPTER padapter);
#endif

#ifdef CONFIG_RTL8188GTV
void rtl8188gtvs_set_hal_ops(PADAPTER padapter);
#endif

#ifdef CONFIG_RTL8192F
void rtl8192fs_set_hal_ops(PADAPTER padapter);
#endif

#endif /* __SDIO_HAL_H__ */
