/******************************************************************************
 * Copyright 2007 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/
#ifndef __RTL8723D_DM_H__
#define __RTL8723D_DM_H__
/* ************************************************************
 * Description:
 *
 * This file is for 8723D dynamic mechanism only
 *
 *
 * ************************************************************ */

/* ************************************************************
 * structure and define
 * ************************************************************ */

/* ************************************************************
 * function prototype
 * ************************************************************ */

void rtl8723d_init_dm_priv(PADAPTER padapter);
void rtl8723d_deinit_dm_priv(PADAPTER padapter);

void rtl8723d_InitHalDm(PADAPTER padapter);
void rtl8723d_HalDmWatchDog(PADAPTER padapter);

#endif
