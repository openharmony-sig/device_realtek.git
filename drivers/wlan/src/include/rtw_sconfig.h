/******************************************************************************
 * Copyright 2007 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/
#ifndef _RTW_SCONFIG_H_
#define _RTW_SCONFIG_H_

struct wifi_profile_t {
	char ssid[64];
	char auth[32];
	char sec[32];
	char key[128];  //password
	char key_id;     //wep key index
	char wep;        //0:open,1:wep,2:wpa/wpa2
};

/*****************************************************************************
function name: rtw_sconfig_start
Function:  start simple config
Parameter:  custom_pin_code:8~64Byte pin code
return:	0:SUCCESS, -1:FAIL
*****************************************************************************/
int rtw_sconfig_start(char *custom_pin_code);

/*****************************************************************************
function name: rtw_get_wifi_profile
Function:  get wifi_profile_t info.This function will block waiting for a sema.
Parameter:  profile:return struct wifi_profile_t.
return:	0:SUCCESS, -1:FAIL
*****************************************************************************/
int rtw_get_wifi_profile(struct wifi_profile_t *profile);

/*****************************************************************************
function name: rtw_sconfig_stop
Function:  stop simple config.
Parameter:  NONE
return:	0:SUCCESS, -1:FAIL
*****************************************************************************/
int rtw_sconfig_stop(void);

#endif

