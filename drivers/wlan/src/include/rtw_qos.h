/******************************************************************************
 * Copyright 2007 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/


#ifndef _RTW_QOS_H_
#define _RTW_QOS_H_

#define DRV_CFG_UAPSD_VO 	BIT0
#define DRV_CFG_UAPSD_VI 	BIT1
#define DRV_CFG_UAPSD_BK 	BIT2
#define DRV_CFG_UAPSD_BE 	BIT3

#define WMM_IE_UAPSD_VO 	BIT0
#define WMM_IE_UAPSD_VI 	BIT1
#define WMM_IE_UAPSD_BK 	BIT2
#define WMM_IE_UAPSD_BE 	BIT3

#define WMM_TID0 	BIT0
#define WMM_TID1 	BIT1
#define WMM_TID2 	BIT2
#define WMM_TID3 	BIT3
#define WMM_TID4 	BIT4
#define WMM_TID5 	BIT5
#define WMM_TID6 	BIT6
#define WMM_TID7 	BIT7

#define AP_SUPPORTED_UAPSD BIT7
/* TC = Traffic Category,  TID0~7 represents TC */
#define BIT_MASK_TID_TC 0xff
/* TS = Traffic Stream,  TID8~15 represents TS */
#define BIT_MASK_TID_TS 0xff00
#define ALL_TID_TC_SUPPORTED_UAPSD 0xff

struct	qos_priv	{

	unsigned int	  qos_option;	/* bit mask option: u-apsd, s-apsd, ts, block ack...		 */

#ifdef CONFIG_WMMPS_STA
	/* uapsd (unscheduled automatic power-save delivery) = a kind of wmmps */
	u8 uapsd_max_sp_len;
	/* declare uapsd_tid as a bitmap for the uapsd setting of TID 0~15 */
	u16 uapsd_tid;
	/* declare uapsd_tid_delivery_enabled as a bitmap for the delivery-enabled setting of TID 0~7 */
	u8 uapsd_tid_delivery_enabled;
	/* declare uapsd_tid_trigger_enabled as a bitmap for the trigger-enabled setting of TID 0~7 */
	u8 uapsd_tid_trigger_enabled;
	/* declare uapsd_ap_supported to record whether the connected ap  supports uapsd or not */
	u8 uapsd_ap_supported;
#endif /* CONFIG_WMMPS_STA */	

};


#endif /* _RTL871X_QOS_H_ */