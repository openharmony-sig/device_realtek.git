/******************************************************************************
 * Copyright 2007 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/
#ifndef __HAL_SDIO_COEX_H__
#define __HAL_SDIO_COEX_H__

#include <drv_types.h>

#ifdef CONFIG_SDIO_MULTI_FUNCTION_COEX

enum { /* for sdio multi-func. coex */
	SDIO_MULTI_WIFI = 0,
	SDIO_MULTI_BT,
	SDIO_MULTI_NUM
};

bool ex_hal_sdio_multi_if_bus_available(PADAPTER adapter);

#else

#define ex_hal_sdio_multi_if_bus_available(adapter) TRUE

#endif  /* CONFIG_SDIO_MULTI_FUNCTION_COEX */
#endif  /* !__HAL_SDIO_COEX_H__ */

