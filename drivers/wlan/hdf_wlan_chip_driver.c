/******************************************************************************
 * Copyright 2007 - 2021 Realtek Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/
#include "hdf_device_desc.h"
#include "hdf_wifi_product.h"
#include "hdf_wlan_chipdriver_manager.h"
#include "harmony_api.h"

#define HDF_LOG_TAG WIFI_CHIP_RTK

#ifndef WLAN_CHIP_DRIVER_NAME
#error please define your driver name
#endif

#ifndef MODULE_NAME
#error MODULE_NAME is required
#endif

#define TO_STRING(R) #R
#define MARO_VALUE_TO_STRING(R) TO_STRING(R)

#define HDF_WLAN_ENTRY_NAME g_hdfWlanChipDriver##WLAN_CHIP_DRIVER_NAME##Entry

static const char * const REALTEK_DRIVER_NAME = "rtw_wlan";

int rtk_wlan_chip_init(void);
int rtk_wlan_chip_deinit(void);
int rtk_driver_init(struct HdfChipDriver *chipDriver, NetDevice *netDev);
int rtk_driver_deinit(struct HdfChipDriver *chipDriver, NetDevice *netDev);
void rtkMac80211Init(struct HdfChipDriver *chipDriver);


static int32_t InitChip(struct HdfWlanDevice *device)
{
    // Init your chip if you need here
    // This function will be called after your specified chip is detected
    // Note the chip have already powered on and resetted here
    int32_t ret = HDF_SUCCESS;
    if(device ==NULL)
    {
        HDF_LOGE("%s:NULL ptr!", __func__);
        return HDF_FAILURE;
    }
    ret = rtk_wlan_chip_init();
   
    if (ret != 0) {
        HDF_LOGE("%s:Init realtek wifi driver failed!", __func__);
        return ret;
    }
    HDF_LOGI("%s: Chip driver init,%d",__func__,ret);
    return HDF_SUCCESS;
}
static int32_t DeinitChip(struct HdfWlanDevice *device)
{
    // Deinit your chip if you need here
    int32_t ret = HDF_SUCCESS;
    ret = rtk_wlan_chip_deinit();
    if (ret != 0) {
        HDF_LOGE("%s:Deinit failed!ret=%d", __func__, ret);
    }
    return ret;
}

struct HdfChipDriver* BuildChipDriver(struct HdfWlanDevice *device, uint8_t ifIndex)
{
    // Build your drvier for the specified device and net interface
    struct HdfChipDriver *chipdriver = NULL;
    if(device == NULL){
        HDF_LOGE("%s fail : channel is NULL", __func__);
        return NULL;
    }
    chipdriver = (struct HdfChipDriver *)OsalMemCalloc(sizeof(struct HdfChipDriver));
    if (memset_s(chipdriver, sizeof(struct HdfChipDriver), 0, sizeof(struct HdfChipDriver)) != EOK) {
        HDF_LOGE("%s fail: memset_s fail!", __func__);
        OsalMemFree(chipdriver);
        return NULL;
    }

    if (strcpy_s(chipdriver->name, MAX_WIFI_COMPONENT_NAME_LEN, REALTEK_DRIVER_NAME) != EOK) {
        HDF_LOGE("%s fail : strcpy_s fail", __func__);
        OsalMemFree(chipdriver);
        return NULL;
    }
    chipdriver->init = rtk_driver_init;
    chipdriver->deinit = rtk_driver_deinit;
    rtkMac80211Init(chipdriver);
    return chipdriver;
}

void ReleaseChipDriver(struct HdfChipDriver *chipDriver)
{
    if (chipDriver == NULL) {
        return;
    }
    if (strcmp(chipDriver->name, REALTEK_DRIVER_NAME) != 0) {
        HDF_LOGE("%s:Not my driver!", __func__);
        return;
    }
    OsalMemFree(chipDriver);
}

static uint8_t GetMaxIFCount(struct HdfChipDriverFactory *factory)
{
    return 1;
}

static int HdfWlanChipDriverBind(struct HdfDeviceObject *dev)
{
    // This function will be called after loading
    return 0;
}

static int32_t HdfWlanChipDriverInit(struct HdfDeviceObject *device)
{
    static struct HdfChipDriverFactory myFactory = {
        .driverName = MARO_VALUE_TO_STRING(MODULE_NAME),
        .ReleaseFactory = NULL, // This factory does not need to be released
        .InitChip = InitChip,
        .DeinitChip = DeinitChip,
        .Build = BuildChipDriver,
        .Release = ReleaseChipDriver,
        .GetMaxIFCount = GetMaxIFCount
    };
    struct HdfChipDriverManager *driverManager = HdfWlanGetChipDriverMgr();
    if (driverManager == NULL)
    {
        HDF_LOGE("%s: driver manager is NULL!", __func__);
        return HDF_FAILURE;
    }
    HDF_LOGI("%s: Chip driver regist. name=%s", __func__, myFactory.driverName);
    return driverManager->RegChipDriver(&myFactory);
}

static void HdfWlanChipDriverRelease(struct HdfDeviceObject *object)
{
    // This function will be called before driver unloading
    // All registed ChipDriverFactory is released by platform,no need to release here
}

struct HdfDriverEntry HDF_WLAN_ENTRY_NAME = {
    .moduleVersion = 1,
    .Bind = HdfWlanChipDriverBind,
    .Init = HdfWlanChipDriverInit,
    .Release = HdfWlanChipDriverRelease,
    .moduleName = MARO_VALUE_TO_STRING(MODULE_NAME),
};

HDF_INIT(HDF_WLAN_ENTRY_NAME);
