
REALTEK_DRIVERS_ROOT := $(LITEOSTOPDIR)/../../device/realtek/drivers

ifeq ($(LOSCFG_DRIVERS_WLAN_REALTEK), y)
    LITEOS_BASELIB += -lhdf_wlan_chip_driver_realtek 
    LIB_SUBDIRS    +=  $(REALTEK_DRIVERS_ROOT)/wlan  
    LITEOS_BASELIB += -lwlancore
    LITEOS_BASELIB += -lwlanhal	
    LITEOS_LD_PATH += -L$(REALTEK_DRIVERS_ROOT)/wlan/src/core
    LITEOS_LD_PATH += -L$(REALTEK_DRIVERS_ROOT)/wlan/src/hal
endif

